TARGET:=as
CC:=gcc
CFLAGS:=-Wall -pedantic -Werror
CHMOD:=chmod

FLEX:=flex
FLEX_OPTS:=
BISON:=bison
BISON_OPTS:=

ifdef DEBUG
CFLAGS+=-ggdb -DDEBUG
FLEX_OPTS+=-d
BISON_OPTS+=--debug
else
CFLAGS+=-DNDEBUG -O2
endif

all: ${TARGET}

${TARGET}: ${TARGET}.lexer.o ${TARGET}.parser.o util.o hashtable.o parser.util.o op.o expr.o arm.o section.o expr.triggers.o
	${CC} ${CFLAGS} -o "$@" $+
	${CHMOD} +x $@

arm.o: arm.c arm.h

util.o: util.c util.h

hashtable.o: hashtable.c hashtable.h

parser.util.o: parser.util.c parser.util.h

op.o: op.c op.h arm.h

section.o: section.c section.h arm.h

expr.o: expr.c expr.h arm.h expr.triggers.h

expr.triggers.o: expr.triggers.c expr.triggers.h section.h expr.h

${TARGET}.lexer.o: ${TARGET}.lexer.c

${TARGET}.lexer.c: ${TARGET}.l ${TARGET}.parser.h
	${FLEX} ${FLEX_OPTS} -o "$@" $<

${TARGET}.parser.o: ${TARGET}.parser.c ${TARGET}.parser.h 

${TARGET}.parser.c ${TARGET}.parser.h: ${TARGET}.y
	${BISON} ${BISON_OPTS} --defines="${TARGET}.parser.h" -o "${TARGET}.parser.c" $<

debug:
	@make --no-print-directory DEBUG=1

clean:
	rm -f ${TARGET} ${TARGET}.lexer.c ${TARGET}.parser.* *.o

test: ${TARGET}
	/arm/bin/arm-linux-as test.s
	/arm/bin/arm-linux-nm -a a.out 
	/arm/bin/arm-linux-objdump -D a.out 
	@echo; echo "--------";echo;
	./${TARGET} test.s

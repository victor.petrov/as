#ifndef EXPR_TRIGGER_H
#define EXPR_TRIGGER_H

#include "section.h"

struct TExpression;

/* Expr callbacks */
typedef void(*ExprCallback)(struct TExpression* e, Section* s, unsigned int loc, void* data);

/* Defines an Expr callback and its params */
typedef struct TExprTrigger
{
    struct TExpression* expr;   /* the Expr for which the callback is registered */
    struct TExpression* target; /* the first param passed to the callback,usually the 'expr' above */
    ExprCallback callback;
    Section* section;
    unsigned int loc;
    void* data;

    unsigned int fired;
} ExprTrigger;

/* A collection of ExprTrigger callbacks */
typedef struct TExprTriggerList
{
    unsigned int count;
    ExprTrigger** list;

} ExprTriggerList;


/* Creates a new ExprTrigger */
ExprTrigger* ExprTrigger_new(struct TExpression* e, ExprCallback cb, Section* s, unsigned int loc, void* data);

/* Adds a new Expr trigger */
ExprTrigger* Expr_addTrigger(struct TExpression* e, ExprCallback cb, Section* s, unsigned int loc, void* data);

/* Adds a new Expr trigger */
ExprTrigger* Expr_addTriggerFor(struct TExpression* e, struct TExpression* target, ExprCallback cb, Section* s, unsigned int loc, void* data);

/** Appends a new ExprTrigger to the trigger list */
void ExprTriggerList_addTrigger(ExprTriggerList* triggers, ExprTrigger* trigger);


/* Call all callbacks */
void ExprTriggerList_dispatchAll(ExprTriggerList* triggers);
#endif


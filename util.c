#include "util.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h> /* delete me */

#define MAX_CHAR 127
#define MAX_UINT 4294967295u

extern void yywarn(const char* s);

/* Converts escape codes to internal representations.
 * Param 'code' needs to contain only the escape letter,
 * i.e. one of the following: n,r,b,t,a,f,b,v,',"",?
 */
char unescape(const char code)
{
    switch (code)
    {
        case 'n':   return '\n';
        case 't':   return '\t';
        case 'b':   return '\b';
        case 'r':   return '\r';
        case 'f':   return '\f';
        case '\\':  return '\\';
        case '\'':  return '\'';
        case '"':   return '\"';
        case 'a':   return '\a';
        case '?':   return '\?';
        case 'v':   return '\v';
        default:    return code;
    }
}

/* Converts octal/hex escape codes to internal representation of chars
 * code needs to contain a pointer to the start of the string,
 * such as '007' or 'FF'
 */
char unescapeNumber(const char* num, int base)
{
	long int val=0;
	char result=0;

	val=strtol(num,NULL,base);

	if ((val>MAX_CHAR) || (val<0))
	{
		yywarn("Numeric escape sequence out of range.");
		result=MAX_CHAR;
	}
	else
		/* Implementation defined. p.31, Steele */
		result=(char)val;

	return result;
}

/* Converts octal/dec/hex number string to unsigned int
* code needs to contain a pointer to the start of the string,
* such as '007' or 'FFFF'
*/
unsigned int toUInt(const char* num, int base)
{
	unsigned long int val=0;
	unsigned int result=0;

	val=strtoul(num,NULL,base);

	if ((val>MAX_UINT) || (val<0))
	{
		yywarn("numeric constant out of range.");
		result=MAX_UINT;
	}
	else
		result=(unsigned int)val;

	return result;
}

char* strduplicate(const char* source, size_t length)
{
    char* result=NULL;

    result=malloc(length+1);

    if (!result)
        return NULL;

    memcpy(result,source,length);
    result[length]='\0';

    return result;
}

char* strappend(char* dest, const char* source, size_t length)
{
    char* result=NULL;
    size_t srclen=0;

    /* realloc accepts NULL */
    if (dest)
        srclen=strlen(dest);

    result=realloc(dest,srclen+length+1);
    printf("new strlen with NULL: %u\n",(unsigned)(srclen+length+1));
    memcpy(result+srclen,source,length);
    result[srclen+length]='\0';

    return result;
}



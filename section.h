#ifndef SECTION_H
#define SECTION_H

#include <stddef.h>

typedef enum TSectionType
{
    SECTION_DATA,
    SECTION_TEXT
} SectionType;


/* Describes a section to write */
typedef struct TSection
{
    unsigned char* data;     /* the actual data */
    size_t size;            /* index of last byte written */
    size_t memsize;
    unsigned int loc;
    unsigned int offset;

    SectionType type;
} Section;


Section* Section_new(SectionType t);
/** emits 'size' bytes at current location */
unsigned int Section_emitS(Section* s, const char* string,size_t size);
/** emits at most 4 bytes at current location */
unsigned int Section_emit(Section* s, unsigned int value, unsigned int nbytes);
/** emits at most 4 bytes at specified location */
unsigned int Section_emitAt(Section* s, unsigned int loc, unsigned int value, unsigned int nbytes);

void Section_dump(Section* s);
#endif


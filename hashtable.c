#include "hashtable.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef HTSIZE
#define HTSIZE 9997
#endif


static unsigned int Hashtable_hash(const char* s);

/** Creates a new Hashtable */
Hashtable* Hashtable_new()
{
    Hashtable* result=NULL;

    /* Allocate the Hashtable object */
    result=malloc(sizeof(*result));
    if (!result)
        return NULL;

    /* Allocate the table that will contain all data */
    result->table=calloc(HTSIZE, sizeof(void*));
    if (!result->table)
    {
        free(result);
        return NULL;
    }

    result->size=HTSIZE;
    result->count=0;

    return result;
}

/** Retrieves the value of a key from the hashtable */
void* Hashtable_get(Hashtable* t, const char* key)
{
    unsigned int index=0;

    assert(t);
    assert(key);

    index=Hashtable_hash(key) % t->size;

    return t->table[index];
}

/** Returns 1 if the hashtable contains the specified key */
unsigned int Hashtable_containsKey(Hashtable* t, const char* key)
{
    return (Hashtable_get(t,key)!=NULL);
}

/** Store a key=value pair in the hashtable */
void Hashtable_put(Hashtable* t, const char* key, void* value)
{
    unsigned int index=0;

    assert(t);
    assert(key);
    assert(value);

    index=Hashtable_hash(key) % t->size;

    /* Update number of elements in the table */
    if (t->table[index]==NULL)
        t->count++;

    t->table[index]=value;
}

/** Remove a key from the hashtable */
void* Hashtable_remove(Hashtable* t, const char* key)
{
    unsigned int index=0;
    void* result=NULL;

    assert(t);
    assert(key);

    /* Hash the key */
    index=Hashtable_hash(key) % t->size;
    /* Save current value */
    result=t->table[index];
    /* Erase value */
    t->table[index]=NULL;

    /* Return original value */
    return result;
}

/** Prints the hashtable */
void Hashtable_print(Hashtable* t,void (*printer)(unsigned int,void*))
{
    int i=0;

    for (i=0;i<t->size;++i)
        if (t->table[i]!=NULL)
            (*printer)(i,t->table[i]);
}


/** Hash function */
static unsigned int Hashtable_hash(const char* key)
{
    unsigned int result=0;
    unsigned int c;

    while ((c=*key++))
        result=result*9 ^ c;

    return result;
}


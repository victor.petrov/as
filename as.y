%error-verbose

%{
    #include <assert.h>
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include "arm.h"
    #include "hashtable.h"
    #include "parser.util.h"
    #include "op.h"
    #include "expr.h"
    #include "section.h"

    extern int yylex();
    extern int yyerror(const char* s);
    extern int yywarn(const char* s);
    extern int yyleng;

    extern char* mif_file;

    static Hashtable* L;
    static Hashtable* R;
    static Section* sections[2]={NULL,NULL};
    static Section* S;                /* current section */
    Expr* e;
    Expr* patchLDR;

    #define LOCATION (S->loc+=ARM_ISIZE)

%}

%token  <ui>
        OP_ADC
        OP_ADD
        OP_AND
        OP_B
        OP_BL
        OP_BIC
        OP_CMN
        OP_CMP
        OP_EOR
        OP_LDR
        OP_MLA
        OP_MOV
        OP_MRS
        OP_MSR
        OP_MUL
        OP_MVN
        OP_ORR
        OP_RSB
        OP_RSC
        OP_SBC
        OP_STR
        OP_SUB
        OP_TEQ
        OP_TST
        OP_NOP

%token
        EOL
        S_COLON
        S_HASH
        S_DASH
        S_PLUS
        S_EQUAL
        S_COMMA
        S_BANG
        S_LEFT_BRACKET
        S_RIGHT_BRACKET

        DIR_ALIGN
        DIR_ALIAS
        DIR_ASCII
        DIR_BALIGN
        DIR_BYTE
        DIR_DATA
        DIR_DEBUG
        DIR_EQU
        DIR_EQUIV
        DIR_EXTERN
        DIR_GLOBAL
        DIR_HWORD
        DIR_ORG
        DIR_PRINT
        DIR_SKIP
        DIR_STRING
        DIR_TEXT
        DIR_WORD

        ASR
        LSL
        LSR
        ROR
        RRX

%token  <ui>
        CONST_NUM
        CONST_CHAR

%token  <s>
        CONST_STRING
        ID
        LABEL

%type  <o>
        Branch
        Move
        Compare
        Logical
        Arithmetic
        Multiply
        Status
        Instruction
        Load
        Store

%type  <ui>
        NumericConstant
        Shift
        RegShift
        ZeroOffset
        ShiftOffset
        PreIndexedShiftOffset
        PostIndexedShiftOffset

%type  <x>
        Label
        Directive
        Expression
        ExpressionList
        StringList
        ProgramRelativeOffset
        FlexOp2
        Offset
        PreIndexedOffset
        PostIndexedOffset


%union
{
    unsigned int ui;
    char c;
    char* s;
    struct TExpression* x;
    struct TOp* o;
}

%initial-action
{
    /* create the .data section */
    sections[SECTION_DATA]=Section_new(SECTION_DATA);
    /* reserve space in the .data section for a branch */
    Section_emit(sections[SECTION_DATA],0,ARM_ISIZE);

    /* create the .text section */
    sections[SECTION_TEXT]=Section_new(SECTION_TEXT);
    S=sections[SECTION_TEXT];

    L=Hashtable_new();
    R=Hashtable_new();
    setupRegs(R);

    patchLDR=Expr_new(3);
    patchLDR->section=sections[SECTION_DATA];
}


%%

Start: Source   { 
                  /* Die if there are undefined symbols at this point */
                  Hashtable_print(L,checkUndefined);

                  /* align the .data section */
                  directiveBAlign(sections[SECTION_DATA],ARM_ISIZE,0);

                  /* Patch all LDR instructions with registered triggers */
                  ExprTriggerList_dispatchAll(&patchLDR->triggers);

                  /* branch over .data */
                  branchOverData(sections[SECTION_DATA]);

                  /* print result */
                  Hashtable_print(L,printExpr);
                  Section_dump(sections[SECTION_DATA]);
                  Section_dump(sections[SECTION_TEXT]);

                  exportToMIF(mif_file,sections,2);
                }
     ;

Source: AsmStatement 
      | AsmStatement Source 
      ;

AsmStatement: EOL
            | LabeledStatement EOL
            | Statement EOL
            ;

LabeledStatement: Label S_COLON LabeledStatement
                | Label S_COLON EOL LabeledStatement
                | Label S_COLON Statement
                | Label S_COLON EOL Statement
                ;

Label:  LABEL   {
                    $$=Label_withLocation(L,$1,S); 
                }
     ;

Statement:  Directive
         |  Instruction { LOCATION; }
         ;

Directive:  DIR_ALIAS   ID S_COMMA ID   { directiveAlias(R,$2,$4); }
         |  DIR_ALIGN                   { directiveAlign(S,0,0); }
         |  DIR_ALIGN   NumericConstant { directiveAlign(S,$2,0); }
         |  DIR_ALIGN   NumericConstant S_COMMA NumericConstant 
                                        { directiveAlign(S,$2,$4); }
         |  DIR_BALIGN                  { directiveAlign(S,0,0); }
         |  DIR_BALIGN  NumericConstant { directiveBAlign(S,$2,0); }
         |  DIR_BALIGN  NumericConstant S_COMMA NumericConstant 
                                        { directiveBAlign(S,$2,$4); }
         |  DIR_ASCII   StringList      { emitAll(S,$2); }
         |  DIR_BYTE    ExpressionList  { emitAllAsType(S,$2,TYPE_BYTE); }
         |  DIR_DATA                    { S=sections[SECTION_DATA]; }
         |  DIR_DEBUG   CONST_STRING    { 
                                          if (strchr($2,'a')||strchr($2,'l'))
                                          {
                                              Hashtable_print(L,printExpr);
                                              printf("\n");
                                          }

                                          if (strchr($2,'a')||strchr($2,'d'))
                                              Section_dump(sections[SECTION_DATA]);

                                          if (strchr($2,'a')||strchr($2,'t'))
                                              Section_dump(sections[SECTION_TEXT]);
                                        }
         |  DIR_EQU     ID S_COMMA Expression   { directiveEqu(S,L,$2,$4); }
         |  DIR_EQUIV   ID S_COMMA Expression   { directiveEquiv(S,L,$2,$4); }
         |  DIR_EXTERN  ID              { Label_newRef(L,$2); }
         |  DIR_GLOBAL  ID              { Label_newRef(L,$2); }
         |  DIR_HWORD   ExpressionList  { emitAllAsType(S,$2,TYPE_HALFWORD); }
         |  DIR_WORD    ExpressionList  { emitAllAsType(S,$2,TYPE_WORD);}
         |  DIR_ORG     Expression      { directiveOrg(S,$2,0); }
         |  DIR_ORG     Expression S_COMMA NumericConstant
                                        { directiveOrg(S,$2,$4); }
         |  DIR_PRINT   CONST_STRING    { printf("%s\n",$2); }
         |  DIR_SKIP    NumericConstant { directiveSkip(S,$2,0); }
         |  DIR_SKIP    NumericConstant S_COMMA NumericConstant
                                        { directiveSkip(S,$2,$4); }
         |  DIR_STRING  StringList      { Expr_setSZ($2); emitAll(S,$2);
                                        
 }
         |  DIR_TEXT                    { 
                                          S=sections[SECTION_TEXT]; 
                                        }
         |  ID S_EQUAL  Expression      { directiveEqu(S,L,$1,$3); }
         ;

Instruction: Branch 
           | Load  
           | Store
           | Arithmetic 
           | Logical 
           | Move
           | Compare
           | Multiply
           | Status
           | OP_NOP { /* NOP = MOV r0,r0 */
                      $$=Op_withBits($1|OPC_MOV);
                      Section_emitAt(S,S->loc,$$->bits,ARM_ISIZE); 
                    } 
           ;

/*********
* BRANCH *
*********/

Branch: OP_B  ProgramRelativeOffset { $$=Op_B($1,S,$2); }
      | OP_BL ProgramRelativeOffset { $$=Op_B($1|LINK,S,$2); }
      ;

/*****************
* LOAD AND STORE *
*****************/

/* p.4-6 LDR STR */
Load:   OP_LDR ID S_COMMA S_EQUAL Expression        { 
                                                      $$=pseudoLDR(sections[SECTION_DATA],patchLDR,$1|ADR_RD(reg(R,$2)),S,$5);
                                                    }
    |   OP_LDR ID S_COMMA ZeroOffset                { $$=Op_withBits($1|OPC_LDR|ADR_RD(reg(R,$2))|$4);
                                                      Section_emitAt(S,S->loc,$$->bits,ARM_ISIZE);
                                                    }
    |   OP_LDR ID S_COMMA PreIndexedOffset          { $$=Op_Mem($1|OPC_LDR|ADR_RD(reg(R,$2)),S,$4); }
    |   OP_LDR ID S_COMMA PreIndexedShiftOffset     { $$=Op_withBits($1|OPC_LDR|ADR_RD(reg(R,$2))|$4);
                                                      Section_emitAt(S,S->loc,$$->bits,ARM_ISIZE);
                                                    }
    |   OP_LDR ID S_COMMA ProgramRelativeOffset     { /* same as PreIndexedOffset with PC:  ldr r0,[pc,#NUM] */
                                                      $$=Op_MemPC(patchLDR,$1|OPC_LDR|PRE_INDEXED|ADR_RN(ARM_PC)|ADR_RD(reg(R,$2)),S,$4);

                                                    }
    |   OP_LDR ID S_COMMA PostIndexedOffset         { $$=Op_Mem($1|OPC_LDR|ADR_RD(reg(R,$2)),S,$4); }
    |   OP_LDR ID S_COMMA PostIndexedShiftOffset    { $$=Op_withBits($1|OPC_LDR|ADR_RD(reg(R,$2))|$4);
                                                      Section_emitAt(S,S->loc,$$->bits,ARM_ISIZE);
                                                    }  
    ;

Store:  OP_STR ID S_COMMA ZeroOffset                { $$=Op_withBits($1|OPC_STR|ADR_RD(reg(R,$2))|$4);
                                                      Section_emitAt(S,S->loc,$$->bits,ARM_ISIZE);
                                                    }
     |  OP_STR ID S_COMMA PreIndexedOffset          { $$=Op_Mem($1|OPC_STR|ADR_RD(reg(R,$2)),S,$4); }
     |  OP_STR ID S_COMMA PreIndexedShiftOffset     { $$=Op_withBits($1|OPC_STR|ADR_RD(reg(R,$2))|$4);
                                                      Section_emitAt(S,S->loc,$$->bits,ARM_ISIZE);
                                                    }
     |  OP_STR ID S_COMMA ProgramRelativeOffset     { /* same as PreIndexedOffset with PC:  str r0,[pc,#NUM] */
                                                      $$=Op_MemPC(patchLDR,$1|OPC_STR|PRE_INDEXED|ADR_RN(ARM_PC)|ADR_RD(reg(R,$2)),S,$4);
                                                    }
     |  OP_STR ID S_COMMA PostIndexedOffset         { $$=Op_Mem($1|OPC_STR|ADR_RD(reg(R,$2)),S,$4); }
     |  OP_STR ID S_COMMA PostIndexedShiftOffset    { $$=Op_withBits($1|OPC_STR|ADR_RD(reg(R,$2))|$4);
                                                      Section_emitAt(S,S->loc,$$->bits,ARM_ISIZE);
                                                    } 
     ;

/******************
* DATA PROCESSING *
******************/

/* p.4-27 ADD SUB RSB ADC SBC RSC */
Arithmetic: OP_ADD ID S_COMMA FlexOp2               { $$=Op_Data($1|OPC_ADD|ADR_RN(reg(R,$2))|ADR_RD(reg(R,$2)),S,$4); }
          | OP_ADD ID S_COMMA ID S_COMMA FlexOp2    { $$=Op_Data($1|OPC_ADD|ADR_RN(reg(R,$4))|ADR_RD(reg(R,$2)),S,$6); }
          | OP_SUB ID S_COMMA FlexOp2               { $$=Op_Data($1|OPC_SUB|ADR_RN(reg(R,$2))|ADR_RD(reg(R,$2)),S,$4); }
          | OP_SUB ID S_COMMA ID S_COMMA FlexOp2    { $$=Op_Data($1|OPC_SUB|ADR_RN(reg(R,$4))|ADR_RD(reg(R,$2)),S,$6); }
          | OP_RSB ID S_COMMA FlexOp2               { $$=Op_Data($1|OPC_RSB|ADR_RN(reg(R,$2))|ADR_RD(reg(R,$2)),S,$4); }
          | OP_RSB ID S_COMMA ID S_COMMA FlexOp2    { $$=Op_Data($1|OPC_RSB|ADR_RN(reg(R,$4))|ADR_RD(reg(R,$2)),S,$6); }
          | OP_ADC ID S_COMMA FlexOp2               { $$=Op_Data($1|OPC_ADC|ADR_RN(reg(R,$2))|ADR_RD(reg(R,$2)),S,$4); }
          | OP_ADC ID S_COMMA ID S_COMMA FlexOp2    { $$=Op_Data($1|OPC_ADC|ADR_RN(reg(R,$4))|ADR_RD(reg(R,$2)),S,$6); }
          | OP_SBC ID S_COMMA FlexOp2               { $$=Op_Data($1|OPC_SBC|ADR_RN(reg(R,$2))|ADR_RD(reg(R,$2)),S,$4); }
          | OP_SBC ID S_COMMA ID S_COMMA FlexOp2    { $$=Op_Data($1|OPC_SBC|ADR_RN(reg(R,$4))|ADR_RD(reg(R,$2)),S,$6); }
          | OP_RSC ID S_COMMA FlexOp2               { $$=Op_Data($1|OPC_RSC|ADR_RN(reg(R,$2))|ADR_RD(reg(R,$2)),S,$4); }
          | OP_RSC ID S_COMMA ID S_COMMA FlexOp2    { $$=Op_Data($1|OPC_RSC|ADR_RN(reg(R,$4))|ADR_RD(reg(R,$2)),S,$6); }
          ;

/* p.4-30 AND ORR EOR BIC */
Logical:    OP_AND ID S_COMMA FlexOp2               { $$=Op_Data($1|OPC_AND|ADR_RN(reg(R,$2))|ADR_RD(reg(R,$2)),S,$4); }
       |    OP_AND ID S_COMMA ID S_COMMA FlexOp2    { $$=Op_Data($1|OPC_AND|ADR_RN(reg(R,$4))|ADR_RD(reg(R,$2)),S,$6); }
       |    OP_ORR ID S_COMMA FlexOp2               { $$=Op_Data($1|OPC_ORR|ADR_RN(reg(R,$2))|ADR_RD(reg(R,$2)),S,$4); }
       |    OP_ORR ID S_COMMA ID S_COMMA FlexOp2    { $$=Op_Data($1|OPC_ORR|ADR_RN(reg(R,$4))|ADR_RD(reg(R,$2)),S,$6); }
       |    OP_EOR ID S_COMMA FlexOp2               { $$=Op_Data($1|OPC_EOR|ADR_RN(reg(R,$2))|ADR_RD(reg(R,$2)),S,$4); }
       |    OP_EOR ID S_COMMA ID S_COMMA FlexOp2    { $$=Op_Data($1|OPC_EOR|ADR_RN(reg(R,$4))|ADR_RD(reg(R,$2)),S,$6); }
       |    OP_BIC ID S_COMMA FlexOp2               { $$=Op_Data($1|OPC_BIC|ADR_RN(reg(R,$2))|ADR_RD(reg(R,$2)),S,$4); }
       |    OP_BIC ID S_COMMA ID S_COMMA FlexOp2    { $$=Op_Data($1|OPC_BIC|ADR_RN(reg(R,$4))|ADR_RD(reg(R,$2)),S,$6); }
       ;

/* p.4-32 MOV MVN */
Move:   OP_MOV ID S_COMMA FlexOp2   { $$=Op_Data($1|OPC_MOV|ADR_RD(reg(R,$2)),S,$4); }
    |   OP_MVN ID S_COMMA FlexOp2   { $$=Op_Data($1|OPC_MVN|ADR_RD(reg(R,$2)),S,$4); }
    ;

/* p.4-34 CMP CMN TST TEQ (SAVE bit always on) */
Compare:    OP_CMP ID S_COMMA FlexOp2 { $$=Op_Data($1|OPC_CMP|SAVE|ADR_RN(reg(R,$2)),S,$4); }
       |    OP_CMN ID S_COMMA FlexOp2 { $$=Op_Data($1|OPC_CMN|SAVE|ADR_RN(reg(R,$2)),S,$4); }
       |    OP_TST ID S_COMMA FlexOp2 { $$=Op_Data($1|OPC_TST|SAVE|ADR_RN(reg(R,$2)),S,$4); }
       |    OP_TEQ ID S_COMMA FlexOp2 { $$=Op_Data($1|OPC_TEQ|SAVE|ADR_RN(reg(R,$2)),S,$4); }
       ;

/* p.4-32 MUL MLA */
Multiply:   OP_MUL ID S_COMMA ID S_COMMA ID             { $$=Op_MUL($1|OPC_MUL,S,reg(R,$2),reg(R,$4),reg(R,$6),0); }
        |   OP_MUL ID S_COMMA ID                        { $$=Op_MUL($1|OPC_MUL,S,reg(R,$2),reg(R,$4),reg(R,$2),0); }
        |   OP_MLA ID S_COMMA ID S_COMMA ID S_COMMA ID  { $$=Op_MUL($1|OPC_MLA,S,reg(R,$2),reg(R,$4),reg(R,$6),reg(R,$8)); }
        ;

/* p.4-72,73 */
Status: OP_MRS ID                                 { /* non gnu as */
                                                    $$=Op_withBits($1|OPC_MRS|ADR_RD(reg(R,$2))); 
                                                    Section_emitAt(S,S->loc,$$->bits,ARM_ISIZE);
                                                  } 
      | OP_MRS ID S_COMMA ID                      { if (strcmp(CPSR,$4))
                                                        yyerror("only the CPSR register is supported at the moment");
                                                    $$=Op_withBits($1|OPC_MRS|ADR_RD(reg(R,$2)));
                                                    Section_emitAt(S,S->loc,$$->bits,ARM_ISIZE); 
                                                  }
      | OP_MSR ID                                 { /* non gnu as */
                                                    $$=Op_withBits($1|OPC_MSR|ADR_RM(reg(R,$2))); 
                                                    Section_emitAt(S,S->loc,$$->bits,ARM_ISIZE); 
                                                  }
      | OP_MSR ID S_COMMA ID                      { if (strcmp(CPSR_f,$2)) 
                                                        yyerror("only the CPSR_f flags are supported at the moment");
                                                    $$=Op_withBits($1|OPC_MSR|ADR_RM(reg(R,$4)));
                                                    Section_emitAt(S,S->loc,$$->bits,ARM_ISIZE); 
                                                  }
      | OP_MSR S_HASH NumericConstant             { /* non gnu as */
                                                    $$=Op_withBits($1|OPC_MSR|mode1ImmediateValue($3));
                                                    Section_emitAt(S,S->loc,$$->bits,ARM_ISIZE); 
                                                  }
      | OP_MSR ID S_COMMA S_HASH NumericConstant  { if (strcmp(CPSR_f,$2))
                                                        yyerror("only the CPSR_f flags are supported at the moment");
                                                    $$=Op_withBits($1|OPC_MSR|mode1ImmediateValue($5));
                                                    Section_emitAt(S,S->loc,$$->bits,ARM_ISIZE); 
                                                  }
      ;
        
/***********************
* OFFSETS AND OPERANDS *
***********************/

/* p.4-24 */
FlexOp2:    S_HASH Expression   { $$=$2; $$->imm=1; }
       |    ID                  { $$=Expr_new(reg(R,$1)); }
       |    ID S_COMMA Shift    { $$=Expr_new(reg(R,$1) | $3); }
       |    ID S_COMMA RegShift { $$=Expr_new(reg(R,$1) | $3); }
       ;

/* OFFSETS: p.4-7 */

/* [Rn] */
ZeroOffset: S_LEFT_BRACKET ID S_RIGHT_BRACKET   { $$=PRE_INDEXED|POSITIVE_OFFSET|ADR_RN(reg(R,$2)); }
          ;

/* [Rn, Offset]{!} */
PreIndexedOffset:   S_LEFT_BRACKET ID S_COMMA Offset S_RIGHT_BRACKET        { $$=$4; $$->bits|=PRE_INDEXED|ADR_RN(reg(R,$2)); }
                |   S_LEFT_BRACKET ID S_COMMA Offset S_RIGHT_BRACKET S_BANG { $$=$4; $$->bits|=PRE_INDEXED|ADR_RN(reg(R,$2))|STORE_OFFSET; }
                ;

/* [Rn, FlexOffset]{!} */
PreIndexedShiftOffset: S_LEFT_BRACKET ID S_COMMA ShiftOffset S_RIGHT_BRACKET        { $$=$4|PRE_INDEXED|ADR_RN(reg(R,$2)); }
                     | S_LEFT_BRACKET ID S_COMMA ShiftOffset S_RIGHT_BRACKET S_BANG { $$=$4|PRE_INDEXED|ADR_RN(reg(R,$2))|STORE_OFFSET; }
                     ;

/* [Rn], Offset */
PostIndexedOffset: S_LEFT_BRACKET ID S_RIGHT_BRACKET S_COMMA Offset { $$=$5; $$->bits|=ADR_RN(reg(R,$2)); }
                 ;

/* [Rn], FlexOffset */
PostIndexedShiftOffset: S_LEFT_BRACKET ID S_RIGHT_BRACKET S_COMMA ShiftOffset { $$=$5|ADR_RN(reg(R,$2)); }
                      ;

/* Special pre-indexed form: LABEL => [pc, #offset] */
ProgramRelativeOffset:  Expression
                     ;

/* p.4-13, p.4-9
* #expr
* {-}Rm
*/
Offset: S_HASH Expression { $$=$2; $$->imm=1; }
      | ID                { $$=Expr_new(ADR_RM(reg(R,$1))|POSITIVE_OFFSET|REG_OFFSET); }
      | S_PLUS ID         { $$=Expr_new(ADR_RM(reg(R,$2))|POSITIVE_OFFSET|REG_OFFSET); }
      | S_DASH ID         { $$=Expr_new(ADR_RM(reg(R,$2))|REG_OFFSET); }
      ;

/* p.4-9
 * {-}Rm, shift
 */
ShiftOffset: ID S_COMMA Shift           { $$=ADR_RM(reg(R,$1))|REG_OFFSET|POSITIVE_OFFSET|$3; }
           | S_PLUS ID S_COMMA Shift    { $$=ADR_RM(reg(R,$2))|REG_OFFSET|POSITIVE_OFFSET|$4; }
           | S_DASH ID S_COMMA Shift    { $$=ADR_RM(reg(R,$2))|REG_OFFSET|$4; }
           ;

/* p.4-9 */
Shift:  ASR S_HASH NumericConstant { if ($3>SHIFT_MAX)
                                         yyerror("shift expression is too large");
                                     $$=($3)?SHIFT_ASR_IMM | $3 << SHIFT_IMM : 0;
                                   }
     |  LSL S_HASH NumericConstant { if ($3>SHIFT_MAX)
                                         yyerror("shift expression is too large");
                                     $$=$3 << SHIFT_IMM;
                                   }
     |  LSR S_HASH NumericConstant { if ($3>SHIFT_LSR_MAX)
                                         yyerror("shift expression is too large");
                                     $$=($3)?SHIFT_LSR_IMM | $3 << SHIFT_IMM : 0;
                                   }
     |  ROR S_HASH NumericConstant { if ($3>SHIFT_MAX)
                                         yyerror("shift expression is too large");
                                     $$=($3)?SHIFT_ROR_IMM | $3 << SHIFT_IMM : 0;
                                   }
     |  RRX                        { $$=SHIFT_RRX_IMM; }
     ;
        
/* p.4-24 */
RegShift:   ASR ID  { unsigned int r=reg(R,$2);
                      if (r==ARM_PC) yywarn("specifying PC as the shift register has UNPREDICTABLE results");
                      $$=SHIFT_ASR_IMM | SHIFT_BY_REG_BIT | ADR_RS(r);
                    }
        |   LSL ID  { unsigned int r=reg(R,$2);
                      if (r==ARM_PC) yywarn("specifying PC as the shift register has UNPREDICTABLE results");
                      $$=SHIFT_LSL_IMM | SHIFT_BY_REG_BIT | ADR_RS(r);
                    }
        |   LSR ID  { unsigned int r=reg(R,$2);
                      if (r==ARM_PC) yywarn("specifying PC as the shift register has UNPREDICTABLE results");
                      $$=SHIFT_LSR_IMM | SHIFT_BY_REG_BIT | ADR_RS(r);
                    }

        |   ROR ID  { unsigned int r=reg(R,$2);
                      if (r==ARM_PC) yywarn("specifying PC as the shift register has UNPREDICTABLE results");
                      $$=SHIFT_ROR_IMM | SHIFT_BY_REG_BIT | ADR_RS(r);
                    }
        ;


/****************************
* CONSTANTS AND EXPRESSIONS *
****************************/

ExpressionList: Expression                        { $$=$1; }
              | ExpressionList S_COMMA Expression { $$=$1; Expr_append($1,$3); }
              ;

Expression:     ID              { $$=Label_newRef(L,$1); }
          |     NumericConstant { $$=Expr_new($1); $$->section=S; }
          ;

NumericConstant:    CONST_NUM
               |    S_DASH CONST_NUM { $$=(0-$2); }
               |    S_PLUS CONST_NUM { $$=$2; }
               |    CONST_CHAR       { $$=(unsigned int)yylval.c; }
               ;

StringList: CONST_STRING                    { $$=Expr_newString($1,yyleng); }
          | StringList S_COMMA CONST_STRING { $$=$1; Expr_append($1,Expr_newString($3,yyleng)); }
          ;

%%


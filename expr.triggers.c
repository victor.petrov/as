#include "expr.triggers.h"

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include "expr.h"

extern int yyerror(const char* s);

/* Creates a new ExprTrigger */
ExprTrigger* ExprTrigger_new(struct TExpression* e,ExprCallback cb,Section* s,
                             unsigned int loc, void* data)
{
    ExprTrigger* result=NULL;

    assert(e);
    assert(s);
    assert(cb);

    result=malloc(sizeof(*result));

    if (!result)
        yyerror("failed to allocate memory");

    result->expr=e;
    result->target=e;
    result->callback=cb;
    result->section=s;
    result->loc=loc;
    result->data=data;
    result->fired=0;

    return result;
}

/** Creates and adds a new ExprTrigger to the Expr */
ExprTrigger* Expr_addTrigger(struct TExpression* e, ExprCallback cb, 
                             Section* s, unsigned int loc, void* data)
{
    ExprTrigger* result=NULL;

    result=ExprTrigger_new(e,cb,s,loc,data);
    
    ExprTriggerList_addTrigger(&e->triggers,result);

    return result;
}

/** Creates and adds a new ExprTrigger to the Expr */
ExprTrigger* Expr_addTriggerFor(struct TExpression* e, struct TExpression* target,
                             ExprCallback cb, Section* s, unsigned int loc, 
                             void* data)
{
    ExprTrigger* result=NULL;

    result=ExprTrigger_new(e,cb,s,loc,data);
    if (target)
        result->target=target;
    
    ExprTriggerList_addTrigger(&e->triggers,result);

    return result;
}

/** Appends a new ExprTrigger to the trigger list */
void ExprTriggerList_addTrigger(ExprTriggerList* triggers,ExprTrigger* trigger)
{
    unsigned int count=0;
    assert(triggers);
    assert(trigger);

    count = ++triggers->count;
    triggers->list=realloc(triggers->list,count * sizeof(trigger));

    triggers->list[count-1]=trigger;
}

/* Call all callbacks */
void ExprTriggerList_dispatchAll(ExprTriggerList* triggers)
{
    unsigned int i=0;
    ExprTrigger* t=NULL;
    ExprCallback cb=NULL;

    assert(triggers);

    /* nothing to dispatch? */
    if (!triggers->list)
        return;

    /* loop over all triggers */
    for (i=0;i<triggers->count;++i)
    {
        /* get and check the trigger */
        t=triggers->list[i];  assert(t);

        /* has it been fired once already? skip it */
        if (t->fired)
            continue;

        /* get and check the callback */
        cb=t->callback;   assert(cb);

        /* set "fired" prior to calling the callback, to prevent loops */
        t->fired=1;

        /* fire! */
        cb(t->target,t->section,t->loc,t->data);
    }
}

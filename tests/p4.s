.text
.extern	BIT_INPUTFLUSH

.global _start
.global _stop

BUFFER = 0xA0000F00                     @ qemu
BUFFER_SIZE = 256

_start: 
	b 		main						@ call main()
	.include "functions.s"				@ include all functions

main:
    mov     a1, #BIT_INPUTFLUSH         @ flush input bit
    bl      flush						@ flush input

	ldr		a1, =BUFFER					@ address of buffer
	ldr     a2, =BUFFER_SIZE            @ buffer size
	bl		read_string					@ read string, strlen returned in r0

_stop:  
    b       _stop                       @ loop forever

.end

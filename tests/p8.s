.data
MSG_MULTIPLICAND: .string "Please input the multiplicand: "
MSG_MULTIPLIER:   .string "Please input the multiplier: "
MSG_RESULT:       .string "Result: "

.text
.global _start
.global _stop
.extern BIT_INPUTFLUSH
.extern BIT_OUTPUTFLUSH

_start: 
	b 		main						@ call main()
	.include "functions.s"				@ include all functions

main:
    ldr     a1, =BIT_INPUTFLUSH         @ prepare to flush the input
    bl      flush                       @ call flush(input)

                                        @ --- multiplicand ---
    ldr     a1, =MSG_MULTIPLICAND       @ display first message
    bl      print_string                @ call print_string()

    ldr     a1, =BIT_OUTPUTFLUSH        @ prepare to flush the output
    bl      flush                       @ call flush(output)

    bl      read_integer                @ call read_integer()
    mov     v5, a1                      @ store r0 in r8

                                        @ --- multiplier ---
    ldr     a1, =MSG_MULTIPLIER         @ display second message
    bl      print_string                @ call print_string()

    ldr     a1, =BIT_OUTPUTFLUSH        @ prepare to flush the output
    bl      flush                       @ call flush(output)

    bl      read_integer                @ call read_integer()
    mov     v6, a1                      @ store r0 in r9

    ldr     a1, =MSG_RESULT             @ display result message
    bl      print_string                @ call print_string()

    mul     a1, v5, v6                  @ r0 = r8 * r9
    bl      print_integer16             @ print the value of r0

    ldr     a1, =BIT_OUTPUTFLUSH        @ prepare to flush the output
    bl      flush                       @ call flush(output)

_stop:  
    b       _stop                       @ loop forever

.end

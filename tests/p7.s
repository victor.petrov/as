.data
N = -2
M = 4

.text
.global _start
.global _stop

_start: 
	b 		main						@ call main()
	.include "functions.s"

main:
    ldr     a1, =N  @ load and sign extend first 16 bit integer 
    mov     a1, a1, LSL #16
    mov     a1, a1, ASR #16

    ldr     a2, =M @ load and sign extend second 16 bit integer
    mov     a2, a2, LSL #16
    mov     a2, a2, ASR #16

    mul     a1, a2 @ multiply the two numbers, place result in r0

_stop:  
    b       _stop                       @ loop forever

.end

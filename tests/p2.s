.text
.extern BIT_INPUTFLUSH

.global _start
.global _stop

_start: 
	b 		main						@ call main()
	.include "functions.s"				@ include all functions

main:
    mov     a1, #BIT_INPUTFLUSH         @ flush input bit
    bl      flush						@ flush input

	bl		read_char					@ read char in r0

_stop:  
    b       _stop                       @ loop forever

.end

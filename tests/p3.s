.data
STRING: .string "Hello, world!\n"
.align

.text
.extern	BIT_OUTPUTFLUSH

.global _start
.global _stop

_start: 
	b 		main						@ call main()
	.include "functions.s"				@ include all functions

main:
	ldr		a1, =STRING					@ the string to print
	bl		print_string				@ call print_string(r0)

	mov		a1, #BIT_OUTPUTFLUSH		@ flush output bit
	bl		flush						@ call flush(r0)

_stop:  
    b       _stop                       @ loop forever

.end

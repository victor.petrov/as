.text
.extern BIT_OUTPUTFLUSH

.global _start
.global _stop

_start: 
	b 		main						@ call main()
	.include "functions.s"				@ include all functions

main:
    mov     a1, #'A'                    @ the character to print
    bl      print_char                  @ call print_char()

    mov     a1, #BIT_OUTPUTFLUSH        @ flush output
    bl      flush						@ call flush()

_stop:  
    b       _stop                       @ loop forever (qemu)

.end

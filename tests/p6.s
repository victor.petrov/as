.text
.global _start
.global _stop

_start: 
	b 		main						@ call main()
	.include "functions.s"				@ include all functions

main:
    ldr a1, =BIT_INPUTFLUSH             @ prepare to flush the input stream
    bl  flush                           @ call flush(input)

    bl  read_integer                    @ read int, result=r0,status=r1

_stop:  
    b       _stop                       @ loop forever

.end

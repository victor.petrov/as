.text
NUMBER=0x8000

.global _start
.global _stop
.extern BIT_OUTPUTFLUSH

_start: 
	b 		main						@ call main()
	.include "functions.s"				@ include all functions

main:
    ldr a1, =NUMBER                     @ load NUMBER to print
    bl  print_integer16                 @ call print_integer16(NUMBER)

    ldr a1, =BIT_OUTPUTFLUSH            @ prepare to flush the output
    bl  flush                           @ call flush(output)

_stop:  
    b       _stop                       @ loop forever

.end

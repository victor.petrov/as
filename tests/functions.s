.data
CARRIAGE_RETURN = '\r'
LINE_FEED = '\n'
MINUS = '-'
ASCII_ZERO_OFFSET = 0x30

ONE_OVER_TEN_16BIT = 0x199A             @ ceil(0.1 * 2^16)
ONE_OVER_HUNDRED_16BIT = 0x290          @ ceil(0.01 * 2^16) 
ONE_OVER_THOUSAND_16BIT = 0x42          @ ceil(0.001 * 2^16)
ONE_OVER_TEN_THOUSAND_16BIT = 0x7       @ ceil(0.0001 * 2^16)
TEN_THOUSAND=10000
THOUSAND=1000
HUNDRED=100
TEN=10

REG_IOCONTROL = 0x00FF00
REG_IOBUFFER  = 0x00FF02

@REG_IOCONTROL = 0xA0000000              @ qemu
@REG_IOBUFFER  = 0xA0000002              @ qemu

BIT_INPUTREADY = 1
BIT_OUTPUTREADY = 2
BIT_INPUTFLUSH = 1
BIT_OUTPUTFLUSH = 2

.text
.global print_char
.global print_string
.global read_char
.global flush

@---------------------------------------
@ void wait(const unsigned int a1)
@
@ Waits until the I/O device is ready
@ \param a1 Either BIT_INPUTREADY or
@                  BIT_OUTPUTREADY
@ \warning  Changes CPSR, a2, a3
@ \note     Does not change a1
@---------------------------------------
wait:
    ldr a2, =REG_IOCONTROL
    _wait_loop:
        ldrb    a3, [a2]                    @ r2=lo(*r1)
        tst     a3, a1                      @ bit set?
        beq     _wait_loop                  @ wait until it's set
    mov pc, lr

@---------------------------------------
@ void print_char(unsigned char a1)
@
@ Prints 1 character to REG_IOBUFFER
@ Waits for I/O device to become ready
@ \param a1 The char to write
@ \warning  Changes CPSR,a1-a4,v1
@---------------------------------------
print_char:
    mov     v1, a1                      @ save the char to print
    mov     a4, lr                      @ save return address
    mov     a1, #BIT_OUTPUTREADY        @ wait output bit
    bl      wait                        @ wait until output is ready
    ldr     a1, =REG_IOBUFFER           @ r0 = &REG_IOBUFFER
    strb    v1, [a1]                    @ store lo(r0) in REG_IOBUFFER 
    mov     pc, a4                      @ return

@---------------------------------------
@ void print_string(unsigned char* s)
@ \param a1 Address of null-terminated
@           string
@ \warning Changes CPSR,a1-a4,v1-v3
@---------------------------------------
print_string:
    mov     v2, a1                      @ r4 = r0
    mov     v3, lr                      @ r5 = lr
    _print_string_loop:
        ldrb    a1, [v2], #1                @ r1 = *r4; ++r4
        cmp     a1, #0                      @ r1==0?
        moveq   pc, v3                      @ return early
        bl      print_char                  @ call print_char(a1)
        b       _print_string_loop          @ loop

@---------------------------------------
@ void print_integer16(signed short int i)
@ \param a1 A signed 2 byte integer
@ \warning Changes CPSR, a1-a4, v1-v4
@---------------------------------------
print_integer16:
    mov     v5, lr                          @ r7 = lr
    movs    a2, a1, LSL #16                 @ r1 = lo16(r0) (see movmi)
    mov     v2, a2, ASR #16                 @ r5 = sign_extend(r1)
    bpl     _print_integer16_positive
                                            @ -- print minus sign ---
    mvn   a2, #0                            @ r1 = -1
    mul   v2, a2                            @ r5 = abs(r5), for div10_16()
    mov   a1, #MINUS                        @ arg1 = '-'
    bl    print_char                        @ call print_char('-')

    _print_integer16_positive:              @ --- ten thousand ---
    mov     a2, #ONE_OVER_TEN_THOUSAND_16BIT
    mov     a1, v2                          @ restore r0
    bl      div10_16                        @ divide by 10000
    mov     v3, a1                          @ save digit
    adds    v4, a1                          @ v4 = jump taken in the past
    beq     _print_integer16_thousands      @ nothing to print
    add     a1, #ASCII_ZERO_OFFSET          @ int to ascii digit
    bl      print_char                      @ call print_char(r0)

    ldr     a2, =TEN_THOUSAND               @ prepare to subtract ten thousands
    mul     a2, v3                          @ r1 = how many tens of thousand
    sub     v2, a2                          @ r5 = last 4 digits of i

    _print_integer16_thousands:             @ --- thousand ---
    mov     a2, #ONE_OVER_THOUSAND_16BIT
    mov     a1, v2                          @ restore r0
    bl      div10_16                        @ divide by 1000
    mov     v3, a1                          @ save digit
    adds    v4, a1                          @ if (r0==0), compare v4 and r0
    beq     _print_integer16_hundreds       @ then jump to hundreds
    add     a1, #ASCII_ZERO_OFFSET          @ int to ascii digit
    bl      print_char                      @ call print_char(r0)

    mov     a2, #THOUSAND                   @ prepare to subtract thousands
    mul     a2, v3                          @ r1 = how many thousand
    sub     v2, a2                          @ r5 = last 3 digits of i

    _print_integer16_hundreds:              @ --- hundred ---
    ldr     a2, =ONE_OVER_HUNDRED_16BIT
    mov     a1, v2                          @ restore r0
    bl      div10_16                        @ divide by 100
    mov     v3, a1                          @ save digit
    adds    v4, a1                          @ if (r0==0), compare v4 and r0
    beq     _print_integer16_tens           @ then jump to tens
    add     a1, #ASCII_ZERO_OFFSET          @ int to ascii digit
    bl      print_char                      @ call print_char(r0)

    mov     a2, #HUNDRED                    @ prepare to subtract hundreds
    mul     a2, v3                          @ r1 = how many thousand
    sub     v2, a2                          @ r5 = last 2 digits of i

    _print_integer16_tens:                  @ --- tens ---
    ldr     a2, =ONE_OVER_TEN_16BIT
    mov     a1, v2                          @ restore r0
    bl      div10_16                        @ divide by 10
    mov     v3, a1                          @ save digit
    adds    v4, a1                      @ if (r0==0), compare v4 and r0
    beq     _print_integer16_digit          @ then jump to digit
    add     a1, #ASCII_ZERO_OFFSET          @ int to ascii digit
    bl      print_char                      @ call print_char(r0)

    mov     a2, #TEN                        @ prepare to subtract tens
    mul     a2, v3                          @ r1 = how many thousand
    sub     v2, a2                          @ r0 = last digit of i

    _print_integer16_digit:                 @ --- last digit ---
    mov     a1, v2                          @ r0 = v2
    add     a1, #ASCII_ZERO_OFFSET          @ int to ascii digit
    bl      print_char                      @ call print_char(r0)

    mov   pc, v5                            @ return

@---------------------------------------
@ unsigned char read_char()
@ \return unsigned char in r0
@ \warning Changes CPSR, a1-a4
@---------------------------------------
read_char:
    mov     a4, lr
    mov     a1, #BIT_INPUTREADY         @ will check if input is ready
    bl      wait                        @ call wait(input)
    ldr     a1, =REG_IOBUFFER           @ r0=&REG_IOBUFFER
    ldrb    a1, [a1]                    @ r0=lo(*r0)
    mov     pc, a4                      @ return

@-------------------------------------------------------------------
@ unsigned int read_string(unsigned char* buffer, unsigned int size)
@ \warning Changes CPSR,a1-a4,v1-v4
@ \note Sets CPSR based on result length
@-------------------------------------------------------------------
read_string:
    mov     v1, #0                      @ r4 = 0 (result length)
    mov     v2, lr                      @ r5 = lr (return address)
    mov     v3, a1                      @ r6 = r0 (&buffer)
    movs    v4, a2                      @ r7 = r1 (size)
    beq     _read_string_end            @ return if size == 0

    _read_string_loop:
        bl      read_char
        cmp     a1, #LINE_FEED              @ r0 == line feed?
        cmpne   a1, #CARRIAGE_RETURN        @ if not LF, check r0 == CR
        beq     _read_string_end            @ break
        strb    a1, [v3], #1                @ *r6 = r0; ++r6
        add     v1, #1                      @ ++r4 (string length)
        cmp     v1, v4                      @ length < buffer size?
        blt     _read_string_loop

    _read_string_end:
    movs    a1, v1                      @ string length
    mov     pc, v2                      @ return
        
@---------------------------------------
@ signed int read_integer()
@ \warning Changes CPSR,a1-a4,v1-v4
@---------------------------------------
read_integer:
    mov     v1, lr                      @ r4 = lr
    mov     v2, #0                      @ r5 = guard: digits and -
    mov     v3, #0                      @ r6 = result = 0
    mov     v4, #0                      @ r7 = 0 (only digits expected)
    _read_integer_loop:
        bl      read_char                   @ call read_char()
        cmp     a1, #LINE_FEED              @ r0 == line feed?
        cmpne   a1, #CARRIAGE_RETURN        @ if not LF, check r0 == CR
        bne     _read_integer_minus         @ if not, check for minus
        cmp     v4, #0                      @ no digits yet?
        beq     _read_integer_loop          @ if no digits, read next char
        b       _read_integer_result        @ else, return result

        _read_integer_minus:
        cmp     v2, #0                      @ minus allowed?
        bne     _read_integer_digit         @ if not, check digit
        cmp     a1, #MINUS                  @ check if char is MINUS
        movne   v2, #1                      @ not MINUS? r5=1 (result >= 0)
        bne     _read_integer_digit         @ not MINUS? check digit
        mvn     v2, #0                      @ (r0==MINUS)? r5=-1 (result<0)
        b _read_integer_loop                @ read next char

        _read_integer_digit:
        mov     v4, #1                      @ v4 = 1 when we expect a digit
        subs    a1, #ASCII_ZERO_OFFSET      @ r0=ord(r0)
        bmi     _read_integer_invalid_char  @ (r0<0)?return invalid char
        cmp     a1, #TEN                    @ compare a1 and 10
        bhs     _read_integer_invalid_char  @ (r0>9)? return invalid char
        cmp     v2, #0
        mvnmi   a1, a1                      @ complement
        addmi   a1, #1                      @ and increment
        mov     a2, #TEN                    @ prepare to multiply result by 10
        mla     v3, a2, v3, a1              @ r6 = (r6 * 10) + r0
        mov     v3, v3, LSL #16             @ prepare to sign extend
        mov     v3, v3, ASR #16             @ sign extend 16 bit result
        b       _read_integer_loop          @ read next char

    _read_integer_invalid_char:
    add     a1, #ASCII_ZERO_OFFSET      @ r0 = original char
    mov     a2, #0                      @ r1 = 0 (invalid)
    mov     pc, v1                      @ return

    _read_integer_result:
    mov a1, v3                          @ r0 = result
    mov a2, #1                          @ r1 = 1 (valid)
    mov     pc, v1                      @ return
    
@---------------------------------------
@ void flush(const unsigned int in_out)
@ \param a1    Either BIT_INPUTFLUSH or
@              BIT_OUTPUTFLUSH
@ \note Does not change a1
@ \warning Changes a2,a3
@---------------------------------------
flush:
    ldr     a2, =REG_IOCONTROL          @ r1 = &REG_IOCONTROL
    ldrb    a3, [a2]                    @ r2 = lo(*r1)
    orr     a3, a3, a1                  @ r2 = r2 | r0
    strb    a3, [a2]                    @ *r1 = r2
    mov     pc, lr                      @ return

@------------------------------------------
@ unsigned int div_16bit(unsigned int a1,
@                        unsigned int a2)
@ \note Sets CPSR
@ \warning Changes CPSR, a1, a2
@ \return unsigned int in r0
@------------------------------------------
div10_16:
    mul     a2, a1, a2                  @ r0 = r0 * r1; P.S.: Rd != Rm
    movs    a1, a2, ASR #16             @ r0 = r0 >> 16
    mov     pc, lr


#ifndef HASHTABLE_H
#define HASHTABLE_H

#include <stddef.h>

typedef struct THashtable
{
    void** table;
    size_t size;
    size_t count;
} Hashtable;

/** Creates a new Hashtable */
Hashtable* Hashtable_new();

/** Retrieves the value of a key from the hashtable */
void* Hashtable_get(Hashtable* t, const char* key);

/** Returns 1 if the hashtable contains the specified key */
unsigned int Hashtable_containsKey(Hashtable* t, const char* key);

/** Store a key=value pair in the hashtable */
void Hashtable_put(Hashtable* t, const char* key, void*);

/** Remove a key from the hashtable */
void* Hashtable_remove(Hashtable* t, const char* key);

/** Prints the hashtable */
void Hashtable_print(Hashtable* t,void (*printer)(unsigned int,void*));

#endif


#include "expr.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "arm.h"
#include "util.h"

extern int yyerror(const char* s);

/* Creates a new Expr object using a label */
Expr* Expr_newLabel(const char* label)
{
    Expr* result=NULL;

    result=calloc(1,sizeof(*result));
    if (!result)
        yyerror("failed to allocate memory");

    result->label=strduplicate(label,strlen(label));

    return result;
}

/** Creates a new Expr "object" from label and value */
Expr* Expr_newLabelVal(const char* label, unsigned int value)
{
    Expr* result=NULL;

    assert(label);

    result=Expr_newLabel(label);
    Expr_setValue(result,value,TYPE_WORD);

    return result;
}

/** Creates a new Expr "object" using a number */
Expr* Expr_new(unsigned int value)
{
    Expr* result=NULL;

    result=calloc(1,sizeof(*result));
    if (!result)
        yyerror("failed to allocate memory");

    Expr_setValue(result,value,TYPE_WORD);

    return result;
}

/** Creates a new Expr "object" using a number*/
Expr* Expr_newString(char* value, size_t length)
{
    Expr* result=NULL;

    assert(value);

    result=calloc(1,sizeof(*result));
    if (!result)
        yyerror("failed to allocate memory");

    Expr_setStringValue(result,value,length);

    return result;
}

Expr* Expr_copy(Expr* e)
{
    Expr* result=NULL;

    assert(e);

    result=Expr_new(e->value.ui);

    memcpy(result,e,sizeof(*result));

    if (e->type==TYPE_STRING)
        result->value.s=strduplicate(e->value.s,e->size);
    else
        result->value.ui=e->value.ui;

    if (e->label)
        result->label=strduplicate(e->label,strlen(e->label));

    result->previous=NULL;
    result->next=NULL;

    return result;
}

/* Set the numeric value of an expression */
void Expr_setValue(Expr* e, unsigned int value, ExprType type)
{
    assert(e);

    Expr_setType(e,type);
    e->value.ui=value;
    e->defined=1;

    if (e->triggers.count)
        ExprTriggerList_dispatchAll(&e->triggers);
}

/* Set the string value of an expression */
void Expr_setStringValue(Expr* e, char* value, size_t length)
{
    assert(e);

    e->type=TYPE_STRING;
    e->size=length;
    e->value.s=strduplicate(value,length);
    e->defined=1;
}


/* Increments the length of all strings in the expr list */
void Expr_setSZ(Expr* e)
{
    assert(e);

    e=Expr_first(e);

    while (e)
    {
        e->size++;
        e=e->next;
    }
}

/* Set the type and size of an expression */
void Expr_setType(Expr* e, ExprType type)
{
    assert(e);

    e->type=type;

    switch (type)
    {
        case TYPE_WORD: e->size=ARM_WORD; break;
        case TYPE_HALFWORD: e->size=ARM_HALFWORD; break;
        case TYPE_BYTE: e->size=ARM_BYTE; break;
        default: ; /* don't know the size of the string */
    }
}

/* Prepend e2 to e1 */
void Expr_prepend(Expr* e1, Expr* e2)
{
    Expr* first=NULL;

    assert(e1);
    assert(e2);

    first=Expr_first(e1);

    e2->next=first;
    e2->previous=NULL;
    first->previous=e2;
}

Expr* Expr_first(Expr* e)
{
    Expr* first=NULL;

    assert(e);

    first=e;

    while (first->previous)
        first=first->previous;

    return first;
}

Expr* Expr_last(Expr* e)
{
    Expr* last=NULL;

    assert(e);

    last=e;

    while (last->next)
        last=last->next;

    return last;
}

/* Append e2 to e1 */
void Expr_append(Expr* e1, Expr* e2)
{
    Expr* last=NULL;

    assert(e1);
    assert(e2);

    last=Expr_last(e1);

    last->next=e2;
    e2->previous=last;
    e2->next=NULL;
}


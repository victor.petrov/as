#include "arm.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

unsigned int isByteAligned(unsigned int value)
{
	return  !(value & ARM_ALIGN_MASK);
}

/* Checks if an 32 bit immediate is valid acording to
* http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.dui0473c/Chdfgchf.html
* @return Number of rotate operations performed on IMM_MASK
*         or -1 if the immediate value is not valid
*/
int isValidImmediate(unsigned int value)
{
    unsigned int mask=IMM_MASK;
	unsigned int pattern=IMM_MASK;
    int i=0;

    for(i=1;i<=16;++i)
    {
		if (value == (value & pattern))
			return 2*(i-1);

		/* rotate left 2*i */
		pattern=rotater(mask,2*i);
    }

	return -1;
}

/* Shifts an immediate value until it matches IMM_MASK */
unsigned int make8bitImmediate(unsigned int value)
{
	unsigned int result=value;
	int i=0;

	for (i=1;i<16;++i)
	{
		if (result == (result & IMM_MASK))
			return result;

		/* rotate left 2*i */
		result=rotater(result,2*i);
	}

	return 0;
}

/** Rotates a value left by the specified amount */
unsigned int rotatel(unsigned int value, int amount)
{
	return (value<<amount) | (value>>(ARM_WIDTH-amount)); 
}

/** Rotates a value right by the specified amount */
unsigned int rotater(unsigned int value, int amount)
{
	return (value>>amount) | (value<<(ARM_WIDTH-amount)); 
}

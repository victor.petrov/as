#ifndef UTIL_H
#define UTIL_H

#include <stddef.h>

/* Converts escape codes to internal representations.
* Param 'code' needs to contain only the escape letter,
* i.e. one of the following: n,r,b,t,a,f,b,v,',"",?
*/
char unescape(const char code); 

/* Converts octal/hex escape codes to internal representation of chars
* code needs to contain a pointer to the start of the string,
* such as '007' or 'FF'
*/
char unescapeNumber(const char* num, int base);

/* Converts octal/dec/hex number string to unsigned int
* code needs to contain a pointer to the start of the string,
* such as '007' or 'FFFF'
*/
unsigned int toUInt(const char* num, int base);

char* strduplicate(const char* source, size_t length);

char* strappend(char* dest, const char* source, size_t length);

#endif


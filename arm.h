#ifndef ARM_H
#define ARM_H

#define ARM_NREGS 16
#define ARM_WIDTH 32
#define ARM_ISIZE    4 /* bytes */
#define HOST_WORD    4 /* bytes */
#define HOST_HALFWORD 2 /* bytes */
#define ARM_WORD     4 /* bytes */
#define ARM_HALFWORD 2 /* bytes */
#define ARM_BYTE     1 /* byte */
#define ARM_WORD_MASK 0xFFFFFFFF        /* 32 bits make up a word */
#define ARM_HALFWORD_MASK 0x0000FFFF    /* lo(16) bits make up a halfword */
#define ARM_BYTE_MASK 0x000000FF        /* lo(8) bits make up a byte */

#define ARM_AREGS 4         /* a1 - a4 */
#define ARM_AREG_START 0    /* a1 = r0 */
#define ARM_VREGS 8         /* v1 - v8 */
#define ARM_VREG_START 4    /* v1 = r4 */
#define ARM_IP 12
#define ARM_SP 13
#define ARM_LR 14
#define ARM_PC 15

#define ARM_PC_INC 8        /* amount the PC is incremented by */
#define ARM_ALIGN_MASK 0x3

#define ARM_ROT_IMM 8

#define ARM_DATA_RD 12
#define ARM_DATA_RN 16
#define ARM_DATA_RM 0 
#define ARM_DATA_RS 8 

#define ARM_MUL_RD 16
#define ARM_MUL_RN 12
#define ARM_MUL_RM 0 
#define ARM_MUL_RS 8 

#define ADR_RN(x)   ((x) << ARM_DATA_RN)
#define ADR_RD(x)   ((x) << ARM_DATA_RD)
#define ADR_RS(x)   ((x) << ARM_DATA_RS)
#define ADR_RM(x)   ((x) << ARM_DATA_RM)

#define AMR_RN(x)   ((x) << ARM_MUL_RN)
#define AMR_RD(x)   ((x) << ARM_MUL_RD)
#define AMR_RS(x)   ((x) << ARM_MUL_RS)
#define AMR_RM(x)   ((x) << ARM_MUL_RM)

#define SHIFT_MAX 31
#define SHIFT_LSR_MAX 32
#define SHIFT_LSR_MIN 1
#define SHIFT_IMM 7

#define CPSR    "CPSR"
#define CPSR_f  "CPSR_f"

#define ARM_MAX_IMM_OFFSET 4095
#define ARM_MIN_IMM_OFFSET -4095

#define DATA_OFFSET 4   /* save space for a branch over the data section */

/* Instruction condition */
enum OpField
{
    COND_EQ=0x0,
    COND_NE=0x1 << (ARM_WIDTH - 4),
    COND_CS=0x2 << (ARM_WIDTH - 4),
    COND_CC=0x3 << (ARM_WIDTH - 4),
    COND_MI=0x4 << (ARM_WIDTH - 4),
    COND_PL=0x5 << (ARM_WIDTH - 4),
    COND_VS=0x6 << (ARM_WIDTH - 4),
    COND_VC=0x7 << (ARM_WIDTH - 4),
    COND_HI=0x8 << (ARM_WIDTH - 4),
    COND_LS=0x9 << (ARM_WIDTH - 4),
    COND_GE=0xA << (ARM_WIDTH - 4),
    COND_LT=0xB << (ARM_WIDTH - 4),
    COND_GT=0xC << (ARM_WIDTH - 4),
    COND_LE=0xD << (ARM_WIDTH - 4),
    COND_AL=0xE << (ARM_WIDTH - 4),

    SAVE =            0x1 << (ARM_WIDTH - 12),
    BYTE =            0x1 << (ARM_WIDTH - 10),     /* 0 = WORD */
    SIGNED_HALFWORD = 0x1 << (ARM_WIDTH - 26),     /* 0 = UNSIGNED HALFWORD */
    HALFWORD =        0x1 << (ARM_WIDTH - 27),     /* 0 = SIGNED BYTE */

    LINK = 0x1 << (ARM_WIDTH - 8),
    OP_IMM_BIT = 0x1 << (ARM_WIDTH - 7),           /* I bit (mode 1 addressing)*/

    SHIFT_ASR_IMM = 0x1 << 6,
    SHIFT_LSL_IMM = 0x0,
    SHIFT_LSR_IMM = 0x1 << 5,
    SHIFT_ROR_IMM = 0x3 << 5,
    SHIFT_RRX_IMM = 0x3 << 5,                      /* same as ROR */

    SHIFT_BY_REG_BIT = 0x1 << 4,

    PRE_INDEXED=0x1 << (ARM_WIDTH - 8),
    POSITIVE_OFFSET=0x1 << (ARM_WIDTH - 9),
    STORE_OFFSET = 0x1 << (ARM_WIDTH - 11),                 /* bit W in LDR/STR */
    REG_OFFSET = 0x1 << (ARM_WIDTH - 7)
};

enum OpCode
{
    OPC_AND = 0x0,                                  /* 0000 */
    OPC_EOR = 0x1 << (ARM_WIDTH - 11),              /* 0001 */
    OPC_SUB = 0x2 << (ARM_WIDTH - 11),              /* 0010 */
    OPC_RSB = 0x3 << (ARM_WIDTH - 11),              /* 0011 */
    OPC_ADD = 0x4 << (ARM_WIDTH - 11),              /* 0100 */
    OPC_ADC = 0x5 << (ARM_WIDTH - 11),              /* 0101 */
    OPC_SBC = 0x6 << (ARM_WIDTH - 11),              /* 0110 */
    OPC_RSC = 0x7 << (ARM_WIDTH - 11),              /* 0111 */
    OPC_TST = 0x8 << (ARM_WIDTH - 11),              /* 1000 */
    OPC_TEQ = 0x9 << (ARM_WIDTH - 11),              /* 1001 */
    OPC_CMP = 0xA << (ARM_WIDTH - 11),              /* 1010 */
    OPC_CMN = 0xB << (ARM_WIDTH - 11),              /* 1011 */
    OPC_ORR = 0xC << (ARM_WIDTH - 11),              /* 1100 */
    OPC_MOV = 0xD << (ARM_WIDTH - 11),              /* 1101 */
    OPC_BIC = 0xE << (ARM_WIDTH - 11),              /* 1110 */
    OPC_MVN = 0xF << (ARM_WIDTH - 11),              /* 1111 */
    OPC_MUL = 0x9 << 4,                             /* 1001 */
    OPC_MLA = OPC_MUL | (0x1 << (ARM_WIDTH - 11)),  /* 1001 | [21:21] */
    OPC_MRS = (0xF << (ARM_WIDTH - 16)) |           /* 1111 | [24:24] */
              (0x1 << (ARM_WIDTH - 8)),
    OPC_MSR = 0x128F000,                            /* 00010 0 10 1000(field F) 1111 */
    OPC_LDR = ((0x1 << (ARM_WIDTH - 6)) |
              (0x1 << (ARM_WIDTH - 12))),
    OPC_STR = 0x1 << (ARM_WIDTH - 6)

};

enum OpMask
{
    COND_MASK = 0xF << (ARM_WIDTH - 4),
    B_MASK = 0x5 << (ARM_WIDTH - 7),
    B_IMM_MASK = 0xFFFFFF,
    IMM_MASK = 0xFF,
    OFFSET_IMM_MASK=0xFFF
};

/** Returns 1 if a value is byte aligned, 0 otherwise */
unsigned int isByteAligned(unsigned int value);

/** Checks to see if an immediate value is valid for ARM
* @return int On Success: Number of rotations performed on IMM_MASK
*             On Failure: -1
*/
int isValidImmediate(unsigned int value);

/* Shifts an immediate value until it matches IMM_MASK */
unsigned int make8bitImmediate(unsigned int value);

/** Rotates value left by the specified amount */
unsigned int rotatel(unsigned int value, int amount);

/** Rotates a value right by the specified amount */
unsigned int rotater(unsigned int value, int amount);

#endif

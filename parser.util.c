#include "parser.util.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "util.h"

extern void yyerror(const char* s);


void emit(Section* s, Expr* v)
{
    assert(s);
    assert(v);

    switch (v->type)
    {
        case TYPE_STRING:   Section_emitS(s,v->value.s,v->size); break;
        case TYPE_WORD:     
        case TYPE_HALFWORD:
        case TYPE_BYTE:     Section_emit(s,v->value.ui,v->size); break;
    }
}

/* Call emit() on 'l' and all its siblings */
void emitAll(Section* s, Expr* l)
{
    assert(s);
    assert(l);

    l=Expr_first(l);    /* get the first Expr in the list */

    do
    {
        emit(s,l);
        l=l->next;
    }
    while(l);
}

void emitAllAsType(Section* s, Expr* l,ExprType type)
{
    assert(s);
    assert(l);

    l=Expr_first(l);

    do
    {
        Expr_setType(l,type);
        emit(s,l);
        l=l->next;
    }
    while (l);
}

/** Implements the .alias directive */
void directiveAlias(Hashtable* t, const char* r1, const char* r2)
{
    unsigned int reg_id=0;
    Expr* e=NULL;

    assert(t);

    /* check if r1 has already been defined */
    if (Hashtable_containsKey(t,r1))
    {
        fprintf(stderr,"%s: ",r1);
        yyerror("register name is already in use");
    }

    /* get the register number r2 is pointing to */
    e=Hashtable_get(t,r2);

    if (!e)
    {
        fprintf(stderr,"%s: ",r2);
        yyerror("no such register defined");
    }

    reg_id=e->value.ui;

    e=Expr_newLabelVal(r1,reg_id);

    /* create a new entry for r1 with the value of r2 */
    Hashtable_put(t,r1,e);
}


/* Implements the align directive */
unsigned int directiveAlign(Section* s, unsigned int bitpos, unsigned int value)
{
    unsigned int amount=0;

    assert(s);

    if (bitpos)
        amount=0x1 << bitpos;
    else
        amount=ARM_WORD;

    return directiveBAlign(s,amount,value);
}

unsigned int directiveBAlign(Section* s, unsigned int bytes, unsigned value)
{
    assert(s);

    if (!bytes)
        bytes=ARM_WORD;

    if (bytes & (bytes-1))
        yyerror("alignment not a power of two");

    while (s->loc % bytes)
        Section_emit(s,value,ARM_BYTE);

    return bytes;
}


/* Implements the skip directive */
unsigned int directiveSkip(Section* s, unsigned int bytes, unsigned value)
{
    unsigned int i=0;

    assert(s);

    for (i=0;i<bytes;++i)
        Section_emit(s,value,ARM_BYTE);

    return bytes;
}

/* Implements the .org directive. 
   Fills the .text section with NOPs when possible. */
unsigned int directiveOrg(Section* s, Expr* e, unsigned int value)
{
    unsigned int at=0;
    unsigned int bytes=0;
    unsigned int to_align=0;
    unsigned int result=0;

    assert(s);

    at=e->value.ui;
    result = bytes = at - s->loc;  /* how many bytes to skip */

    /* how many bytes till loc is aligned? */
    if (s->loc % ARM_WORD)
        to_align=ARM_WORD - (s->loc % ARM_WORD); 

    /* move to current location? nothing to do */
    if (at == s->loc)
        return 0;

    if (at < s->loc)
        yyerror("attempt to move .org backwards");

    /* for data sections or text sections where org will not align loc */
    if ((s->type==SECTION_DATA) || (bytes<to_align))
    {
        directiveSkip(s,bytes,value);
    }
    else
    {
        /* at this point, we have a text section with enough bytes to skip
           to be able to align the location counter on a word boundary.
           'at' is reused, it will be the number of bytes used to align */
        if (to_align)
            bytes -= directiveSkip(s,to_align,value);

        /* not enough bytes to use NOPs? just pad as before */
        if (bytes<ARM_WORD)
            directiveSkip(s,bytes,value);
        else
        {
            /* insert NOPs (mov r0,r0) */
            while (bytes>=ARM_WORD)
            {
                Section_emit(s,COND_AL|OPC_MOV,ARM_WORD);
                bytes -= ARM_WORD;
            }   

            /* are there more bytes left to fill, but not enough to use NOP? */
            if (bytes)
                directiveSkip(s,bytes,value);
        }
    }

    /* return the number of bytes skipped */
    return result;
}

/* Implements the .equ directive */
Expr* directiveEqu(Section* s,Hashtable* t, const char* label, Expr* e)
{
    Expr* result=NULL;

    assert(t);
    assert(label);
    assert(e);

    result=Label_newRef(t,label);
    result->section=s;

    if (e->defined)
        cbEQU(e,s,s->loc,result);
    else
        Expr_addTrigger(e,cbEQU,s,s->loc,result);

    return result;
}

/* .equ callback */
void cbEQU(Expr* rhs, Section* s, unsigned int loc, void* data)
{
    Expr* lhs=NULL;

    assert(rhs);
    assert(s);
    assert(data);

    /* the left hand side of  .equ l1, l2 */
    lhs=(Expr*)data;
    
    assert(rhs->defined); /* right hand side must be defined now */

    Expr_setValue(lhs,rhs->value.ui,rhs->type);
}

/* Implements the .equv directive */
Expr* directiveEquiv(Section* s,Hashtable* t, const char* label, Expr* e)
{
    Expr* result=NULL;

    assert(t);
    assert(label);
    assert(e);

    result=Label_newRef(t,label);

    if (result->defined)
        yyerror("symbol already defined");

    if (e->defined)
        cbEQU(e,s,s->loc,result);
    else
        Expr_addTrigger(e,cbEQU,s,s->loc,result);


    return result;
}

unsigned int mode1Immediate(Expr* e)
{
    unsigned int result=0;

    if (!e->defined)
        return 0;

    result=mode1ImmediateValue(e->value.ui);

    return result;
}

unsigned int mode1ImmediateValue(unsigned int v)
{
    unsigned int result=0;
    int rot_imm=0;

    rot_imm=isValidImmediate(v);

    if (rot_imm<0)
        yyerror("invalid immediate value");

	/* rotate left to push all bits to the right, it's counter-intuitive,
	   but it works because rot_imm is the amount the mask was rotated right
	   (from LSB) to match the value, and now, if the value is rotated left
	   by the same amount, it will match the starting mask, OFFSET_IMM_MASK */
	result=rotatel(v,rot_imm);

    /* store rot_imm in the result at bits ARM_ROT_IMM+4 */
    result|=((rot_imm/2) << ARM_ROT_IMM);
    result|=OP_IMM_BIT;
    
    return result;
}

/* Checks the value of an immediate.
   ALSO SETS THE POSITIVE_OFFSET BIT */
void mode2Immediate(Expr* e)
{
    int sv=0;

    assert(e);

    if (!e->defined)
        return;

    sv=(int)e->value.ui;

    if ((sv>ARM_MAX_IMM_OFFSET) || (sv<ARM_MIN_IMM_OFFSET))
        yyerror("immediate offset value too large");

    if (sv>0)
        e->bits|=POSITIVE_OFFSET;
    else
        e->value.ui=~e->value.ui+1; /* undo negative values
                                       created by Expression */

    /* Restrict value to 12 bits (3 bytes) */
    e->value.ui &= OFFSET_IMM_MASK;
}

/* Checks the value of an immediate and returns 'bits' */
unsigned int mode2ImmediateValue(unsigned int value)
{
    int sv=0;
    unsigned int result=0;

    sv=(int)value;

    if ((sv>ARM_MAX_IMM_OFFSET) || (sv<ARM_MIN_IMM_OFFSET))
        yyerror("immediate offset value too large");

    if (sv>0)
        result|=POSITIVE_OFFSET;
    else
        value=~value+1; /* undo negative values
                                       created by Expression */

    value&=OFFSET_IMM_MASK;
    result|=value;

    /* Restrict value to 12 bits (3 bytes) */
    return result;
}



/** Creates a new Expr "object" or fetches on from a
* a hashtable, if such label exists */
Expr* Label_withLocation(Hashtable* t, const char* label, Section* s)
{
    Expr* result=NULL;

    assert(t);
    assert(label);
	assert(s);

    result=Label_newRef(t,label);

    if (result->defined)
        yyerror("label already defined");

	result->section=s;
	result->imm=1;
    Expr_setValue(result,s->loc,TYPE_WORD);

    return result;
}


/** Creates a new Expr "object" or fetches on from a
* a hashtable, if such label exists */
Expr* Label_newRef(Hashtable* t, const char* label)
{
    Expr* result=NULL;

    assert(t);
    assert(label);

    result=Hashtable_get(t,label);

    /* if not found, create a new label reference */
    if (!result)
    {
        result=Expr_newLabel(label);
        Hashtable_put(t,label,result);
    }

    return result;
}

/** Returns the register ID from a register symbol */
unsigned int reg(Hashtable* t, const char* reg)
{
    Expr* e;

    assert(t);
    assert(reg);

    e=Hashtable_get(t,reg);
    
    if (!e)
    {
        fprintf(stderr,"%s: ",reg);
        yyerror("no such register");
    }  

    return e->value.ui;
}

/** Adds default ARM registers to the Hashtable */
void setupRegs(Hashtable* t)
{
    int i=0;
    char val[4]={0,0,0,0};

    assert(t);

    /* r R */
    for (i=0;i<ARM_NREGS;++i)
    {
        sprintf(val,"r%d",i);
        Hashtable_put(t,val,Expr_newLabelVal(val,i));

        sprintf(val,"R%d",i);
        Hashtable_put(t,val,Expr_newLabelVal(val,i));
    }

    /* a A */
    for (i=1;i<=ARM_AREGS;++i)
    {
        sprintf(val,"a%d",i);
        Hashtable_put(t,val,Expr_newLabelVal(val,ARM_AREG_START+i-1));

        sprintf(val,"A%d",i);
        Hashtable_put(t,val,Expr_newLabelVal(val,ARM_AREG_START+i-1));
    }

    /* v V */
    for (i=1;i<=ARM_VREGS;++i)
    {
        sprintf(val,"v%d",i);
        Hashtable_put(t,val,Expr_newLabelVal(val,ARM_VREG_START+i-1));

        sprintf(val,"V%d",i);
        Hashtable_put(t,val,Expr_newLabelVal(val,ARM_VREG_START+i-1));
    }

    /* ip IP */
    Hashtable_put(t,"ip",Expr_newLabelVal("ip",ARM_IP));
    Hashtable_put(t,"IP",Expr_newLabelVal("ip",ARM_IP));

    /* sp SP */
    Hashtable_put(t,"sp",Expr_newLabelVal("sp",ARM_SP));
    Hashtable_put(t,"SP",Expr_newLabelVal("SP",ARM_SP));

    /* lr LR */
    Hashtable_put(t,"lr",Expr_newLabelVal("lr",ARM_LR));
    Hashtable_put(t,"LR",Expr_newLabelVal("LR",ARM_LR));

    /* pc PC */
    Hashtable_put(t,"pc",Expr_newLabelVal("pc",ARM_PC));
    Hashtable_put(t,"PC",Expr_newLabelVal("PC",ARM_PC));
}

/* Prints all Expr in a Hashtable */
void printExpr(unsigned int index, void* expr)
{
    Expr* e=expr;
    char section='?';

    if (!e->defined)
        section='U';
    else
        if (e->section->type==SECTION_DATA)
            section='d';
        else
            if (e->section->type==SECTION_TEXT)
                section='t';

    printf("%08x %c %u %s\t\n",e->value.ui,section,e->triggers.count,e->label);

}

/* Emits a branch over the data section at location 0 */
void branchOverData(Section* data)
{
    Op* op=NULL;
    Expr* e=NULL;

    assert(data);

    op=Op_withBits(COND_AL|B_MASK);
    e=Expr_new(data->size);

    cbOp_B(e,data,0,op);
}


/* Checks if there are any undefined symbols left */
void checkUndefined(unsigned int i, void* item)
{
    Expr* e=item;

    if (!e)
        return;

    if (!e->defined)
    {
        fprintf(stderr,"label '%s': ",e->label);
        yyerror("undefined symbol");
    }
}


/* Saves an array of Section's to a MIF file */
void exportToMIF(char* filename, Section** sections, unsigned int count)
{
    FILE* mif=NULL;
    unsigned int i=0;
    Section* s=NULL;
    unsigned char* data=NULL;
    unsigned int bytes_printed=0;
	unsigned int total_bytes=0;


    assert(sections);

    if (!filename)
        filename="out.mif";

    mif=fopen(filename,"w");

    if (!mif)
    {
        fprintf(stderr,"%s: ",filename);
        yyerror("failed to open MIF file for writing");
    }

    fprintf(mif,"-- MIF\n");
    fprintf(mif,"DEPTH = 16384;\n");
    fprintf(mif,"WIDTH = 16;\n");
    fprintf(mif,"ADDRESS_RADIX = HEX;\n");
    fprintf(mif,"DATA_RADIX = HEX;\n");
    fprintf(mif,"CONTENT\nBEGIN\n\n");


    for (i=0;i<count;++i)
    {
        s=sections[i];
        data=(unsigned char*)s->data;
		bytes_printed=0;

        do
        {
            fprintf(mif,"%04X : %02X%02X;\n",total_bytes/2,
					data[bytes_printed+1],data[bytes_printed]);
			bytes_printed+=2;
			total_bytes+=2;
        }
        while (bytes_printed < s->size);
    }

    fprintf(mif,"[%04X..%04X] : %04X;\n",bytes_printed,16383,0);

    fprintf(mif,"\nEND;\n");
    fclose(mif);
}

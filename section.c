#include "section.h"
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include "arm.h"

#define SECTION_GROW_BY 1024

extern int yyerror(const char* s);

static void Section_grow(Section* s);

/** Create a new section with the specified number of bytes and type */
Section* Section_new(SectionType t)
{
    Section* result=NULL;

    result=calloc(1,sizeof(*result));
    if (!result)
        yyerror("failed to allocate memory");

    Section_grow(result);

    result->type=t;
    
    return result;
}

/** Makes room for another value */
static void Section_grow(Section* s)
{
    assert(s);

    s->memsize+=SECTION_GROW_BY;

    s->data=realloc(s->data,s->memsize);
    if (!s->data)
        yyerror("failed to reallocate memory");
}

/** Emits a sequence of bytes at current location */
unsigned int Section_emitS(Section* s, const char* v, size_t size)
{
    unsigned int i=0;

    assert(s);
    assert(v);

    if (s->loc+size>s->memsize)
        Section_grow(s);

    for (i=0;i<size;++i)
        s->data[s->loc++]=v[i];

    /* update section size */
    if (s->loc>s->size)
        s->size=s->loc;

    return s->loc;
}

/** emits at most 4 bytes at current location. Increments section location.  */
unsigned int Section_emit(Section* s, unsigned int value, unsigned int nbytes)
{
    assert(s);
 	s->loc=Section_emitAt(s,s->loc,value,nbytes);

    return s->loc;
}

/** emits at most 4 bytes at specified location. Does not increment the section location. */
unsigned int Section_emitAt(Section* s, unsigned int loc, unsigned int value, unsigned int nbytes)
{
    unsigned int i=0;
    unsigned char byte=0;

    assert(s);

    if (loc+nbytes>s->memsize)
        Section_grow(s);

    for (i=0;i<nbytes;++i)
    {
        byte=(value >> (i*8)) & ARM_BYTE_MASK;
        s->data[loc++]=byte;
    }

	/* update size of section */
    if (loc>s->size)
        s->size=loc;

    return loc;
}

void Section_dump(Section* s)
{
    unsigned int skip=0;
    unsigned int bytes_printed=0;
    /*unsigned int j=0; */
    /*unsigned int p=0;*/ /* entire line = 0? */
    unsigned int word=0;
    unsigned int* data=NULL;

    assert(s);
    assert(s->data);

    if (!s->size)
        return;

    data=(unsigned int*)s->data;

    printf("\n%08x",s->offset);

    switch (s->type)
    {
        case SECTION_DATA:printf(" <.data>"); break;
        case SECTION_TEXT:printf(" <.text>"); break;
    }

    printf(" [%u]:\n",(unsigned int)s->size);

    do
    {
        word=data[bytes_printed/HOST_WORD];

        /* skip all zeros and NOPs */
        if (!word || word==(COND_AL|OPC_MOV))
        {
            /* avoid printing multiple lines with ellipses */
            if (!skip)
                printf("      ...\n");
            skip=1;
            bytes_printed+=HOST_WORD;
            continue;
        }

		skip=0;

        printf("%4x: ",bytes_printed);

        printf("%08x",word);

        bytes_printed+=HOST_WORD;
        printf("\n");
    }
    while (bytes_printed<s->size);
 
}

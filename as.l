%option yylineno warn noyywrap case-insensitive

%{
    #include <assert.h>
    #include <stdio.h>
    #include <stdlib.h>
    #include "arm.h"
    #include "as.parser.h"
    #include "util.h"

    /** Displays an error and exits with error code 1
    * @param message The string to display
    */
    int yyerror(const char* message);

    /** Prints a warning message
    * @param message The warning message
    */
    void yywarn(const char* message);

    static int warned=0;
    static int string_length=0;
    static enum yytokentype instruction=0;
    static unsigned int op=COND_AL;
    extern int yyparse();

    #define INSTRUCTION(x)      \
    {                           \
        op=COND_AL;             \
        instruction=(x);        \
        yymore();               \
    } 

    #define COND(x)             \
    {                           \
        /* Clear COND field */  \
        op&=~COND_MASK;         \
        op|=(x);                \
        yymore();               \
    }

    #define SET(x)              \
    {                           \
        op|=(x);                \
        yymore();               \
    }

    #define RETURN_INSTRUCTION  \
    {                           \
        yylval.ui=op;           \
        return instruction;     \
    }

    typedef struct TBufferStack
    {
        struct TBufferStack* previous;
        YY_BUFFER_STATE state;
        int line;
        char* filename;
        FILE* f;
    } BufferStack;

    int pushfile(char* filename);
    int popfile();

    char* current_filename=NULL;
    BufferStack* cb=NULL;
    static char c;
    char* mif_file=NULL;

%}

%x I
%x IC
%x IDX
%x CHAR
%x STRING
%x IF

eol     [\n\r;]
ws      [\t ]
notws   [^\t ]

hex     0[xX][0-9A-Fa-f]+
nothex  0[xX][^0-9A-Fa-f]+
dec     0|([1-9][0-9]*)
oct     0[0-7]+
notoct  0[89]+
bin     0[bB][01]+
notbin  0[bB][^01]+

esc     [ntbrfv'\\"a?]
octesc  [01][0-7]{0,2}
hexesc  x[0-7][0-9A-Fa-f]{0,1}

label   [A-Za-z0-9_\$][A-Za-z0-9_\$\.]*
id      [A-Za-z0-9_\$\.]
notid   [^A-Za-z0-9_\$\.]

 /* cond    (eq)|(ne)|(cs)|(hs)|(cc)|(lo)|(mi)|(pl)|(vs)|(vc)|(hi)|(ls)|(ge)|(lt)|(gt)|(le)|(al) */

notl    [^A-Za-z]

%%

{label}/:       { yylval.s=strduplicate(yytext,yyleng); 
                  return LABEL;
                }
{label}/{ws}+:  { yywarn("labels must be immediately followed by the ':' character"); 
                  yylval.s=strduplicate(yytext,yyleng);
                  return LABEL; 
                }
adc             { BEGIN(I); INSTRUCTION(OP_ADC); }
add             { BEGIN(I); INSTRUCTION(OP_ADD); }
and             { BEGIN(I); INSTRUCTION(OP_AND); }
b               { BEGIN(I); INSTRUCTION(OP_B);   }
bl              { BEGIN(I); INSTRUCTION(OP_BL);  }
bic             { BEGIN(I); INSTRUCTION(OP_BIC); }
cmn             { BEGIN(I); INSTRUCTION(OP_CMN); }
cmp             { BEGIN(I); INSTRUCTION(OP_CMP); }
eor             { BEGIN(I); INSTRUCTION(OP_EOR); }
ldr             { BEGIN(I); INSTRUCTION(OP_LDR); }
mla             { BEGIN(I); INSTRUCTION(OP_MLA); }
mov             { BEGIN(I); INSTRUCTION(OP_MOV); }
mrs             { BEGIN(I); INSTRUCTION(OP_MRS); }
msr             { BEGIN(I); INSTRUCTION(OP_MSR); }
mul             { BEGIN(I); INSTRUCTION(OP_MUL); }
mvn             { BEGIN(I); INSTRUCTION(OP_MVN); }
orr             { BEGIN(I); INSTRUCTION(OP_ORR); }
rsb             { BEGIN(I); INSTRUCTION(OP_RSB); }
rsc             { BEGIN(I); INSTRUCTION(OP_RSC); }
sbc             { BEGIN(I); INSTRUCTION(OP_SBC); }
str             { BEGIN(I); INSTRUCTION(OP_STR); }
sub             { BEGIN(I); INSTRUCTION(OP_SUB); }
teq             { BEGIN(I); INSTRUCTION(OP_TEQ); }
tst             { BEGIN(I); INSTRUCTION(OP_TST); }
nop             { BEGIN(I); INSTRUCTION(OP_NOP); }

<I>eq           { BEGIN(IC); COND(COND_EQ); }
<I>ne           { BEGIN(IC); COND(COND_NE); }
<I>cs           { BEGIN(IC); COND(COND_CS); }
<I>hs           { BEGIN(IC); COND(COND_CS); }
<I>cc           { BEGIN(IC); COND(COND_CC); }
<I>lo           { BEGIN(IC); COND(COND_CC); }
<I>mi           { BEGIN(IC); COND(COND_MI); }
<I>pl           { BEGIN(IC); COND(COND_PL); }
<I>vs           { BEGIN(IC); COND(COND_VS); }
<I>vc           { BEGIN(IC); COND(COND_VC); }
<I>hi           { BEGIN(IC); COND(COND_HI); }
<I>ls           { BEGIN(IC); COND(COND_LS); }
<I>ge           { BEGIN(IC); COND(COND_GE); }
<I>lt           { BEGIN(IC); COND(COND_LT); }
<I>gt           { BEGIN(IC); COND(COND_GT); }
<I>le           { BEGIN(IC); COND(COND_LE); }
<I>al           { BEGIN(IC); COND(COND_AL); }

<I,IC>{ws}      { BEGIN(INITIAL); RETURN_INSTRUCTION; }
<I,IC>s         {
                    if ((instruction==OP_CMP) || 
                        (instruction==OP_CMN) ||
                        (instruction==OP_TST) ||
                        (instruction==OP_TEQ))
                    {
                        REJECT;
                    }
                    else
                    {
                        SET(SAVE);
                    }
                }
<I,IC>b         { 
                    if ((instruction==OP_LDR) || (instruction==OP_STR))
                    {
                        SET(BYTE);
                    }
                    else
                        REJECT;
                }
<I,IC>sb        {
                    if (instruction!=OP_LDR)
                        REJECT;
                    /* else the SIGNED_BYTE bit will be 0 */
                }
<I,IC>h         {
                    if ((instruction!=OP_LDR) && (instruction!=OP_STR))
                    {
                         REJECT;
                    }
                    else
                         SET(HALFWORD);
                }

<I,IC>sh        {
                    if (instruction!=OP_LDR)
                    {
                        REJECT;
                    }
                    else
                        SET(SIGNED_HALFWORD);
                }
<I>t{ws}        {
                    if (instruction!=OP_BL)
                    {
                        REJECT;
                    }
                    else
                    {
                        /* blt */
                        instruction=OP_B;
                        COND(COND_LT);
                    }
                }
<I,IC>{id}      { BEGIN(IDX); yymore(); }
<I,IC>.|{eol}   { 
                    BEGIN(INITIAL); 
                    unput(yytext[yyleng-1]);
                    RETURN_INSTRUCTION;
                }

ASR{notl}       { unput(yytext[yyleng-1]); return ASR; }
LSL{notl}       { unput(yytext[yyleng-1]); return LSL; }
LSR{notl}       { unput(yytext[yyleng-1]); return LSR; }
ROR{notl}       { unput(yytext[yyleng-1]); return ROR; }
RRX{notl}       { unput(yytext[yyleng-1]); return RRX; }
[A-Za-z_]       { BEGIN(IDX); yymore(); }
<IDX>{id}+      { BEGIN(INITIAL); 
                  yylval.s=strduplicate(yytext,yyleng);
                  return ID;
                }
<IDX>{notid}    { BEGIN(INITIAL); 
                  unput(yytext[yyleng-1]); 
                  yylval.s=strduplicate(yytext,yyleng-1); 
                  return ID; 
                }

\.alias         { return DIR_ALIAS; }
\.align         { return DIR_ALIGN; }
\.balign        { return DIR_BALIGN; }
\.ascii         { return DIR_ASCII; }
\.asciz         { return DIR_STRING; }
\.asciiz        { return DIR_STRING; }
\.byte          { return DIR_BYTE; }
\.data          { return DIR_DATA; }
\.debug         { return DIR_DEBUG; }
\.equ           { return DIR_EQU; }
\.equiv         { return DIR_EQUIV; }
\.end           { yyterminate(); }
\.extern        { return DIR_EXTERN; }
\.global        { return DIR_GLOBAL; }
\.globl         { return DIR_GLOBAL; }
\.hword         { return DIR_HWORD; }
\.include{ws}+["] { BEGIN(IF); }
<IF>[^ \t\n\"]+ { while ((c=input()) && (c!='\n'));
                  if (!pushfile(yytext))
                      yyterminate();
                  BEGIN(INITIAL);
                }
<<EOF>>         { if (!popfile())
                   yyterminate();
                }
\.int           { return DIR_WORD; }
\.org           { return DIR_ORG; }
\.print         { return DIR_PRINT; }
\.set           { return DIR_EQU; }
\.short         { return DIR_HWORD; }
\.skip          { return DIR_SKIP; }
\.space         { return DIR_SKIP; }
\.string        { return DIR_STRING; }
\.text          { return DIR_TEXT; }
\.word          { return DIR_WORD; }
\.[[:alnum:]]+  { yyerror("invalid directive"); }

{bin}           { yylval.ui=toUInt(yytext+2,2); return CONST_NUM; }
{notbin}        { yyerror("invalid binary constant"); }
{oct}           { yylval.ui=toUInt(yytext+1,8); return CONST_NUM; }
{notoct}        { yyerror("invalid octal constant"); }
{dec}           { yylval.ui=toUInt(yytext,10);  return CONST_NUM; }
{hex}           { yylval.ui=toUInt(yytext,16);  return CONST_NUM; }
{nothex}        { yyerror("invalid hexadecimal constant"); }

'               { BEGIN(CHAR); yylval.c='\0';}
<CHAR>\\{esc}   { if(!yylval.c)
                      yylval.c=unescape(*(yytext+1));
                }
<CHAR>\\{octesc} {if(!yylval.c)
                        yylval.c=unescapeNumber(yytext+1,8); 
                }
<CHAR>\\{hexesc} {if(!yylval.c)
                        yylval.c=unescapeNumber(yytext+2,16); 
                }
<CHAR>'         { BEGIN(INITIAL); return CONST_CHAR;}
<CHAR>.         { 
                  if (yylval.c)
                  {
                      if (!warned)
                           yywarn("multicharacter constant");
                       warned=1;
                  }
                  else
                      yylval.c=*yytext;
                }

\"              { BEGIN(STRING); 
                  yylval.s=malloc(1);
                  *yylval.s='\0'; /* NULL terminated string */
                  if (!yylval.s)
                      yyerror("failed to allocate memory");
                  string_length=0;
                }
<STRING>\\{esc} { 
                  yylval.s=realloc(yylval.s,string_length+1);
                  if (!yylval.s)
                      yyerror("failed to allocate memory");
                  yylval.s[string_length++]=unescape(*(yytext+1));
                  yylval.s[string_length]='\0'; /* NULL terminated string */
                }
<STRING>\\{octesc} { 
                  yylval.s=realloc(yylval.s,string_length+1);
                  if (!yylval.s)
                      yyerror("failed to allocate memory");
                  yylval.s[string_length++]=unescapeNumber(yytext+1,8);
                  yylval.s[string_length]='\0'; /* NULL terminated string */
                }
<STRING>\\{hexesc} { 
                  yylval.s=realloc(yylval.s,string_length+1);
                  if (!yylval.s)
                      yyerror("failed to allocate memory");
                  yylval.s[string_length++]=unescapeNumber(yytext+2,16);
                  yylval.s[string_length]='\0'; /* NULL terminated string */
                }
<STRING>[^"\\]+   { 
                  yylval.s=realloc(yylval.s,string_length+yyleng+1);
                  if (!yylval.s)
                      yyerror("failed to allocate memory");
                  memcpy(yylval.s+string_length,yytext,yyleng);
                  string_length+=yyleng;
                  yylval.s[string_length]='\0'; /* NULL terminated string */
                }

<STRING>\"      { BEGIN(INITIAL); yyleng=string_length; return CONST_STRING; }

 /* {id}            { return ID; } */

"+"             { return S_PLUS; }
"-"             { return S_DASH; }
"="				{ return S_EQUAL; }
"#"             { return S_HASH; }
":"             { return S_COLON; }
","             { return S_COMMA; }
"!"             { return S_BANG; }
"["             { return S_LEFT_BRACKET; }
"]"             { return S_RIGHT_BRACKET; }
@.*{eol}+       { return EOL; }
{eol}           { return EOL; }
{ws}+           { /* ignore */ }
.               { fprintf(stderr,"Invalid character '%c' found in ",yytext[0]);
                  yyerror("\n"); }

%%

/** Displays an error and exits with error code 1
* @param message The string to display
*/
int yyerror(const char* message)
{
    if (cb)
        fprintf(stderr,"%s:%d: error: %s\n", cb->filename, yylineno, message);
    else
        fprintf(stderr,"error: %s\n",message);

    exit(EXIT_FAILURE);
}

/** Prints a warning message
* @param message The warning message
*/
void yywarn(const char* message)
{
    if (cb)
        fprintf(stderr,"%s:%d: warning: %s\n",cb->filename,yylineno,message);
    else
        fprintf(stderr,"warning: %s\n",message);
}

int pushfile(char* filename)
{
	FILE* f=NULL;
	BufferStack* bs=NULL;

	bs=malloc(sizeof(*bs));
	if (!bs)
		yyerror("failed to allocate memory");

    if (!filename)
    {
        filename="<input>";
        f=stdin;
    }
    else
    {
        f=fopen(filename,"r");
        if (!f)
        {
            fprintf(stderr,"%s: ",filename);
            yyerror("failed to open file");
        }
    }

	if (cb)
		cb->line=yylineno;

	bs->previous=cb;

	bs->state=yy_create_buffer(f,YY_BUF_SIZE);
	bs->f=f;
	bs->filename=filename;
	yy_switch_to_buffer(bs->state);

	cb=bs;
	yylineno=1;
    current_filename=filename;

    return 1;
}

int popfile(void)
{
  BufferStack *bs = cb;
  BufferStack *prevbs;

  if(!bs)
    return 0;

  /* switch back to previous */
  prevbs = bs->previous;

  if(!prevbs)
    return 0;

  yy_switch_to_buffer(prevbs->state);
  cb = prevbs;
  yylineno = cb->line;
  current_filename = cb->filename;
  return 1; 
}

int main(int argc, char* argv[])
{
    char* filename=NULL;

    if (argc>1)
    {
        if (!strcmp("--help",argv[1]) ||
            !strcmp("-help",argv[1]) ||
            !strcmp("-h",argv[1]) ||
            !strcmp("/?",argv[1]) || /* windows */
            !strcmp("/h",argv[1]))
        {
            printf("Victor Petrov's ARMv5 Assembler v.0.1\n\n");
            printf("Usage: %s [asm-file] [mif-file]\n",argv[0]);
            exit(EXIT_SUCCESS);
        }

        filename=argv[1];

        if (argc>2)
            mif_file=argv[2];
    }

    if (pushfile(filename))
        yyparse();

    return EXIT_SUCCESS;
}

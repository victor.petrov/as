#ifndef EXPR_H
#define EXPR_H

#include <stddef.h>
#include "expr.triggers.h"

/* A forward declaration of Expr */
struct TExpression;

typedef enum TExpressionType
{
    TYPE_WORD,
    TYPE_HALFWORD,
    TYPE_BYTE,
    TYPE_STRING
} ExprType;

/* The Expr object */
typedef struct TExpression
{
    unsigned int bits;
    const char* label;
    unsigned int defined;	/* whether the expr is defined or not */
	Section* section;		/* the section where label was defined */
	unsigned int imm;		/* is immediate ? */
    unsigned int loc;
    size_t size;
    ExprType type;

    union
    {
        unsigned int ui;
        char* s;
    } value;

    struct TExpression* next;
    struct TExpression* previous;

    ExprTriggerList triggers;
} Expr;

/** Creates a new Expr "object" from a label string */
Expr* Expr_newLabel(const char* label);

/** Creates a new Expr "object" from label and value */
Expr* Expr_newLabelVal(const char* label, unsigned int value);

/** Creates a new Expr "object" from an integer*/
Expr* Expr_new(unsigned int value);

/** Creates a new Expr "object" from an integer*/
Expr* Expr_newString(char* value, size_t length);

/** Copy an Expr object */
Expr* Expr_copy(Expr* e);


/* Set the string value of an expression */
void Expr_setStringValue(Expr* e, char* value,size_t length);

/* Increments the length of all strings in the expr list */
void Expr_setSZ(Expr* e);

/* Set the numeric value of an expression */
void Expr_setValue(Expr* e, unsigned int value, ExprType type);

/* Set the type and size of an expression */
void Expr_setType(Expr* e, ExprType type);

/** Prepends e2 to e1 */
void Expr_prepend(Expr* e1, Expr* e2);

/** Appends e2 to e1 */
void Expr_append(Expr* e1, Expr* e2);


Expr* Expr_first(Expr* e);
Expr* Expr_last(Expr* e);

unsigned int Expr_location(Expr* start, Expr* end);

#endif


#ifndef PARSER_UTIL_H
#define PARSER_UTIL_H

#include "arm.h"
#include "expr.h"
#include "section.h"
#include "op.h"
#include "hashtable.h"

/** Implements the .alias directive */
void directiveAlias(Hashtable* t, const char* r1, const char* r2);

/* Implements the align directive */
unsigned int directiveAlign(Section* s, unsigned int pad, unsigned int value);

/* Implements the balign directive */
unsigned int directiveBAlign(Section* s, unsigned int bytes, unsigned value);

/* Implements the skip directive */
unsigned int directiveSkip(Section* s, unsigned int bytes, unsigned value);

/* Implements the .org directive */
unsigned int directiveOrg(Section* s, Expr* e, unsigned int value);

/* Implements the .equ directive */
Expr* directiveEqu(Section* s, Hashtable* t, const char* label, Expr* e);

/* Implements the .equv directive */
Expr* directiveEquiv(Section* s,Hashtable* t, const char* label, Expr* e);

/* .equ callback */
void cbEQU(Expr* rhs, Section* s, unsigned int loc, void* data);

/* Creates a location label */
Expr* Label_withLocation(Hashtable* t, const char* label, Section* s);

/** Creates a new label from an Expr (which is the label value, if defined)
* @warning Label must be unique
*/
Expr* Label_assignExpr(Hashtable* t, const char* label, Expr* e);

/** Creates a new Expr "object" from a label string and fetches its
* value from the hashtable, if appropriate */
Expr* Label_newRef(Hashtable* t, const char* label);

/** Generates an 8 bit rotated immediate
* @warning Immediate value must be a valid ARM immediate or undefined
*/
unsigned int mode1Immediate(Expr* e);

unsigned int mode1ImmediateValue(unsigned int v);

/* Checks the value of an immediate offset for mem ops.
   ALSO SETS THE POSITIVE_OFFSET BIT */
void mode2Immediate(Expr* e);

/* Checks the value of an immediate and returns 'bits' */
unsigned int mode2ImmediateValue(unsigned int value);

/** Returns the register ID associated with a label */
unsigned int reg(Hashtable* t, const char* reg);

/** Sets up default ARM registers */
void setupRegs(Hashtable* t);

/* Prints all Expr in a Hashtable
* @callback for Hashtable_print()
*/
void printExpr(unsigned int index, void* expr);

/* Emits a byte into the current section location. Overwrites value->loc */
void emit(Section* s, Expr* value);

/* Call emit() on 'l' and all its siblings */
void emitAll(Section* s, Expr* l);

/* Change the type of all Expr's and call emit() on 'l' and all its siblings */
void emitAllAsType(Section* s, Expr* l,ExprType type);

/* Emits a branch over the data section at location 0 */
void branchOverData(Section* data);

/* Checks if there are any undefined symbols left */
void checkUndefined(unsigned int i, void* e);

/* Saves an array of Section's to a MIF file */
void exportToMIF(char* filename, Section** sections, unsigned int count);

#endif


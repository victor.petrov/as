#ifndef OP_H
#define OP_H

#include "expr.h"

typedef struct TOp
{
	unsigned int bits;
	unsigned int op;

	struct 
	{
		unsigned int rd;
		unsigned int rn;
		unsigned int rs;
		unsigned int rm;
	} regs;

	struct TOp* next;
	struct TOp* previous;
} Op;

/* Create a new Op */
Op* Op_new();

/* Create a new Op with the specified bits */
Op* Op_withBits(unsigned int bits);

/* Create a new B Op */
Op* Op_B(unsigned int bits, Section* s, Expr* e);

void cbOp_B(Expr* e, Section* s, unsigned int loc, void* data);

/* Create a new Data type instruction */
Op* Op_Data(unsigned int bits, Section* s, Expr* op2);

void cbOp_Data(Expr* e, Section* s, unsigned int loc, void* data);

/* Create a new MUL/MLA type instruction */
Op* Op_MUL(unsigned int bits, Section* s,
		   unsigned int rd, unsigned int rm,
           unsigned int rs, unsigned int rn);

Op* Op_Mem(unsigned int bits, Section* s, Expr* offset);
void cbOp_Mem(Expr* e, Section* s, unsigned int loc, void* data);

Op* Op_MemPC(Expr* patchLDR, unsigned int bits, Section* s, Expr* addr);
void cbOp_MemPC(Expr* addr, Section* s, unsigned int loc, void* data);
void cbOp_patchLDR(Expr* addr, Section* s, unsigned int loc, void* data);

Op* pseudoLDR(Section* ds, Expr* patchLDR, unsigned int bits, Section* s, Expr* e);
#endif


#include "op.h"

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include "arm.h"
#include "parser.util.h"

extern int yyerror(const char* s);

/* Create a new Op */
Op* Op_new()
{
	Op* result=NULL;

	result=calloc(1,sizeof(*result));

	if (!result)
		yyerror("failed to allocate memory");

	return result;
}

Op* Op_withBits(unsigned int bits)
{
    Op* result=NULL;

    result=Op_new();
    result->bits=bits;

    return result;
}

/* Create a new Branch instruction */
Op* Op_B(unsigned int bits, Section* s, Expr* location)
{
	Op* result=NULL;

    assert(s);
    assert(location);

	/* set instruction bits (including the default
	   B_MASK bits */
	result=Op_withBits(bits|B_MASK);

    if (location->defined)
        cbOp_B(location,s,s->loc,result);
    else
        Expr_addTrigger(location,cbOp_B,s,s->loc,result);

    return result;
}

void cbOp_B(Expr* e, Section* s, unsigned int loc, void* data)
{
    unsigned int offset=0;
    Op* op=NULL;
	unsigned int imm=0;

    assert(e);
    assert(e->defined);
    assert(s);
    assert(data);

    op=(Op*)data;

    /* calculate the offset from loc to branch destination */
    offset=e->value.ui - loc;

	/* set the LINK bit */
    if (!isByteAligned(offset))
        yyerror("misaligned branch destination");
    /* reverse the operations done by the ARM CPU */
    imm=(offset-ARM_PC_INC) >> 2;
    imm&=B_IMM_MASK;
    op->bits|=imm;

    Section_emitAt(s,loc,op->bits,ARM_ISIZE);
}

Op* Op_Data(unsigned int bits, Section* s, Expr* op2)
{
	Op* result=NULL;

	assert(op2);

	/* create new op */
	result=Op_withBits(bits);

    if (op2->defined)
        cbOp_Data(op2,s,s->loc,result);
    else
        Expr_addTrigger(op2,cbOp_Data,s,s->loc,result);

    return result;
}

void cbOp_Data(Expr* e, Section* s, unsigned int loc, void* data)
{
    Op* op=NULL;

    assert(e);
    assert(e->defined);
    assert(s);
    assert(data);

    op=(Op*)data;

    if (e->imm)
        op->bits|=mode1ImmediateValue(e->value.ui);
    else
        op->bits|=e->value.ui;

    Section_emitAt(s,loc,op->bits,ARM_ISIZE);
}

/* Create a new MUL/MLA type instruction */
Op* Op_MUL(unsigned int bits, Section* s,
		   unsigned int rd, unsigned int rm, 
           unsigned int rs, unsigned int rn)
{
    Op* result=NULL;

    if (rd==rm)
        yyerror("register Rm and Rd cannot be the same");

    if ((rd==ARM_PC) || (rm==ARM_PC) || (rs==ARM_PC) ||
        (rn==ARM_PC))
        yyerror("r15 is not allowed in MUL or MLA instructions");

    result=Op_withBits(bits|AMR_RD(rd)|AMR_RM(rm)|AMR_RS(rs)|AMR_RN(rn));
    Section_emitAt(s,s->loc,result->bits,ARM_ISIZE);

    return result;
}

/* Create a new Memory accessing instruction */
Op* Op_Mem(unsigned int bits, Section* s, Expr* offset)
{
    Op* result=NULL;

    assert(s);
    assert(offset);

    result=Op_withBits(bits);

    if (offset->defined)
        cbOp_Mem(offset,s,s->loc,result);
    else
        Expr_addTrigger(offset,cbOp_Mem,s,s->loc,result);

    return result;
}

void cbOp_Mem(Expr* e, Section* s, unsigned int loc, void* data)
{
    Op* op=NULL;

    assert(e);
    assert(e->defined);
    assert(s);
    assert(data);

    op=(Op*)data;

    if (e->imm)
        mode2Immediate(e);

    op->bits|=e->bits|e->value.ui;

    Section_emitAt(s,loc,op->bits,ARM_ISIZE);
}

/* Create a new PC Relative memory load/store instruction */
Op* Op_MemPC(Expr* patchLDR, unsigned int bits, Section* s, Expr* addr)
{
    Op* result=NULL;
    unsigned int loc=0;

    assert(patchLDR);
    assert(addr);
    assert(s);

    loc=s->loc;

    result=Op_withBits(bits);

    if (addr->defined)
        cbOp_MemPC(addr,s,s->loc,result);
    else
        Expr_addTrigger(addr,cbOp_MemPC,s,s->loc,result);

    if (addr->section!=s)
    {
        addr->imm=addr->value.ui; /* hack */
        Expr_addTriggerFor(patchLDR,addr,cbOp_patchLDR,s,loc,result);
    }

    return result;
}

void cbOp_MemPC(Expr* addr, Section* s, unsigned int loc, void* data)
{
    unsigned int result=0;
    Op* op=NULL;

    assert(addr);
    assert(addr->defined);
    assert(s);
    assert(data);

    op=(Op*)data;

    /* account for PC inc */
    result=addr->value.ui - ARM_PC_INC - loc;

    if (addr->section!=s)
        result-=addr->section->size;

    /* clear existing immediate bits */
    op->bits&=~OFFSET_IMM_MASK;

    op->bits|=addr->bits|mode2ImmediateValue(result);

    Section_emitAt(s,loc,op->bits,ARM_ISIZE);
}


Op* pseudoLDR(Section* ds, Expr* patchLDR, unsigned int bits, Section* s, Expr* e)
{
    Op* result=NULL;
    Expr* v=NULL;
    unsigned int dsloc=0;

    assert(e);

    v=Expr_copy(e);

    if (!v->defined)
    {
        return Op_withBits(bits);   /* @callback: set op->expr=e; */
    }

    /* @callback aware */

    /* Can use MOV? */
    if (isValidImmediate(v->value.ui)>-1)
    {
        v->imm=1;
        return Op_Data(bits|OPC_MOV,s,v);
    }

    dsloc=ds->loc;
    /* if not, use the real LDR and place the constant in the data section*/
    Section_emit(ds,v->value.ui,v->size);

    /*Expr_setValue(v, dsloc - ds->size - s->loc - ARM_PC_INC,TYPE_WORD); */
    /* Adjust location based on DataSection size */
    if (v->section!=s)
        Expr_setValue(v,dsloc,TYPE_WORD); 
    else
        Expr_setValue(v,dsloc,TYPE_WORD);

    result=Op_MemPC(patchLDR, bits|PRE_INDEXED|OPC_LDR|ADR_RN(ARM_PC),s,v);

    return result;
}

void cbOp_patchLDR(Expr* addr, Section* s, unsigned int loc, void* data)
{
    unsigned int result=0;
    unsigned int dsloc=0;
    Op* op=NULL;

    assert(addr);
    assert(addr->defined);
    assert(s);
    assert(data);

    dsloc=addr->imm; /* hack */
    op=(Op*)data;

    /* compute offset to value */
    result=dsloc - addr->section->size - loc - ARM_PC_INC;

    /* clear existing immediate bits */
    op->bits&=~OFFSET_IMM_MASK;

    op->bits|=addr->bits|mode2ImmediateValue(result);

    Section_emitAt(s,loc,op->bits,ARM_ISIZE);
}
